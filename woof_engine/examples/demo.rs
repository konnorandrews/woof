use woof_engine::{
    interval::IntegerInterval, BaseContext, Binding, Expression, Fact, FunctionContext,
    FunctionContract, Statement, Type,
};

fn main() {
    // fn my_function(arg_a: i32, arg_b: i32) -> i32 {
    //     arg_a + arg_b
    // }

    let contract = FunctionContract {
        facts: vec![
            Fact::Exists(
                Binding::Variable("arg_a".into()),
                Type::Integer(IntegerInterval::new(0, 255)),
            ),
            Fact::Exists(
                Binding::Variable("arg_b".into()),
                Type::Integer(IntegerInterval::new(0, 255)),
            ),
            Fact::Exists(Binding::Return, Type::Integer(IntegerInterval::new(0, 255))),
        ],
        is_pure: true,
        is_coroutine: false,
        is_external: false,
        is_public: false,
    };

    let mut base = BaseContext::new();
    base.add_function_contract([vec!["my_function".to_owned()]], contract.clone());

    let mut context = FunctionContext::new(&base, "my_function".to_owned(), &contract);
    dbg!(context.active_facts().collect::<Vec<_>>());

    context
        .finish(Statement::Expr(Expression::Add(
            Box::new(Expression::Var("arg_a".into())),
            Box::new(Expression::Add(
                Box::new(Expression::Var("arg_a".into())),
                Box::new(Expression::Var("arg_b".into())),
            )),
        )))
        .unwrap();
}
