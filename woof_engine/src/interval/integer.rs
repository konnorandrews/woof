use std::{
    cmp::Ordering,
    ops::{Add, Mul},
};

use num_bigint::BigInt;

mod add;
mod div;
mod inverse;
mod mul;
mod neg;
mod sub;

#[derive(PartialEq, Clone, Eq, Debug)]
pub enum Integer {
    NegativeInfinity,
    Finite(BigInt),
    PositiveInfinity,
}

impl From<BigInt> for Integer {
    fn from(value: BigInt) -> Self {
        Integer::Finite(value)
    }
}

impl Mul for &Integer {
    type Output = Integer;

    fn mul(self, rhs: Self) -> Self::Output {
        use Integer::*;
        match (self, rhs) {
            (NegativeInfinity, NegativeInfinity) => PositiveInfinity,
            (NegativeInfinity, Finite(y)) => match y.cmp(&0.into()) {
                Ordering::Less => PositiveInfinity,
                Ordering::Equal => Finite(0.into()),
                Ordering::Greater => NegativeInfinity,
            },
            (NegativeInfinity, PositiveInfinity) => NegativeInfinity,
            (Finite(x), NegativeInfinity) => match x.cmp(&0.into()) {
                Ordering::Less => PositiveInfinity,
                Ordering::Equal => Finite(0.into()),
                Ordering::Greater => NegativeInfinity,
            },
            (Finite(x), Finite(y)) => Finite(x * y),
            (Finite(x), PositiveInfinity) => match x.cmp(&0.into()) {
                Ordering::Less => NegativeInfinity,
                Ordering::Equal => Finite(0.into()),
                Ordering::Greater => PositiveInfinity,
            },
            (PositiveInfinity, NegativeInfinity) => NegativeInfinity,
            (PositiveInfinity, Finite(y)) => match y.cmp(&0.into()) {
                Ordering::Less => NegativeInfinity,
                Ordering::Equal => Finite(0.into()),
                Ordering::Greater => PositiveInfinity,
            },
            (PositiveInfinity, PositiveInfinity) => PositiveInfinity,
        }
    }
}

impl Ord for Integer {
    fn cmp(&self, other: &Self) -> Ordering {
        use Integer::*;
        match (self, other) {
            (NegativeInfinity, NegativeInfinity) => Ordering::Equal,
            (NegativeInfinity, Finite(_)) => Ordering::Less,
            (NegativeInfinity, PositiveInfinity) => Ordering::Less,
            (Finite(_), NegativeInfinity) => Ordering::Greater,
            (Finite(x), Finite(y)) => x.cmp(y),
            (Finite(_), PositiveInfinity) => Ordering::Less,
            (PositiveInfinity, NegativeInfinity) => Ordering::Greater,
            (PositiveInfinity, Finite(_)) => Ordering::Greater,
            (PositiveInfinity, PositiveInfinity) => Ordering::Equal,
        }
    }
}

impl PartialOrd for Integer {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

// Inclusive integer interval arithmetic.
#[derive(PartialEq, Clone, Eq, Debug)]
pub enum IntegerInterval {
    Empty,
    Single(BigInt),
    Bound { start: BigInt, end: BigInt },
    OpenRight { start: BigInt },
    OpenLeft { end: BigInt },
    Open,
}

impl IntegerInterval {
    pub fn new<S, E>(start: S, end: E) -> Self
    where
        S: Into<BigInt>,
        E: Into<BigInt>,
    {
        Self::Bound {
            start: start.into(),
            end: end.into(),
        }
        .normalize()
    }

    pub fn contains<T>(&self, value: T) -> bool
    where
        T: Into<BigInt>,
    {
        let value = value.into();
        match self {
            IntegerInterval::Empty => false,
            IntegerInterval::Single(x) => x == &value,
            IntegerInterval::Bound { start, end } => start <= &value && &value <= end,
            IntegerInterval::OpenRight { start } => start <= &value,
            IntegerInterval::OpenLeft { end } => &value <= end,
            IntegerInterval::Open => true,
        }
    }

    pub fn is_subinterval_of(&self, value: &IntegerInterval) -> bool {
        use IntegerInterval::*;
        match (self, value) {
            (Empty, _) | (_, Empty) => false,
            (Open, _) | (_, Open) => true,
            (Single(x), Single(y)) => x == y,
            (Single(x), Bound { start, end }) => start <= x && x <= end,
            (Single(x), OpenRight { start }) => start <= x,
            (Single(x), OpenLeft { end }) => x <= end,
            (Bound { start, end }, Single(y)) => start == end && start == y,
            (Bound { start, end }, Bound { start: c, end: d }) => {
                c <= start && start <= d && c <= end && end <= d
            }
            (Bound { start, end }, OpenRight { start: c }) => c <= start && c <= end,
            (Bound { start, end }, OpenLeft { end: d }) => start <= d && end <= d,
            (OpenRight { .. }, Single(_)) => false,
            (OpenRight { .. }, Bound { .. }) => false,
            (OpenRight { start }, OpenRight { start: c }) => c <= start,
            (OpenRight { .. }, OpenLeft { .. }) => false,
            (OpenLeft { .. }, Single(_)) => false,
            (OpenLeft { .. }, Bound { .. }) => false,
            (OpenLeft { .. }, OpenRight { .. }) => false,
            (OpenLeft { end }, OpenLeft { end: d }) => end <= d,
        }
    }

    pub fn contains_infinity(&self) -> bool {
        match self {
            IntegerInterval::Empty => false,
            IntegerInterval::Single(_) => false,
            IntegerInterval::Bound { .. } => false,
            IntegerInterval::OpenRight { .. } => true,
            IntegerInterval::OpenLeft { .. } => false,
            IntegerInterval::Open => true,
        }
    }

    pub fn contains_neg_infinity(&self) -> bool {
        match self {
            IntegerInterval::Empty => false,
            IntegerInterval::Single(_) => false,
            IntegerInterval::Bound { .. } => false,
            IntegerInterval::OpenRight { .. } => false,
            IntegerInterval::OpenLeft { .. } => true,
            IntegerInterval::Open => true,
        }
    }

    fn normalize(self) -> IntegerInterval {
        match self {
            IntegerInterval::Bound { start, end } => match start.cmp(&end) {
                Ordering::Less => IntegerInterval::Bound { start, end },
                Ordering::Equal => IntegerInterval::Single(start),
                Ordering::Greater => IntegerInterval::Empty,
            },
            x => x,
        }
    }

    fn into_options(self) -> (Option<BigInt>, Option<BigInt>) {
        match self {
            IntegerInterval::Empty => {
                panic!("Empty is not representable as an options interval.")
            }
            IntegerInterval::Single(x) => (Some(x.clone()), Some(x)),
            IntegerInterval::Bound { start, end } => (Some(start), Some(end)),
            IntegerInterval::OpenRight { start } => (Some(start), None),
            IntegerInterval::OpenLeft { end } => (None, Some(end)),
            IntegerInterval::Open => (None, None),
        }
    }

    /// Returns the intersection of the intervals.
    pub fn intersection(&self, other: &Self) -> IntegerInterval {
        use IntegerInterval::*;

        if self.is_subinterval_of(other) {
            return self.clone();
        }
        if other.is_subinterval_of(self) {
            return other.clone();
        }

        match (self, other) {
            (Empty, _) | (_, Empty) => Empty,
            (Single(_), _) | (_, Single(_)) => Empty,
            (Open, _) | (_, Open) => unreachable!(),
            (Bound { start: a, end: b }, Bound { start: c, end: d }) => Bound {
                start: a.max(c).clone(),
                end: b.min(d).clone(),
            },
            (Bound { start: a, end: b }, OpenRight { start: c }) => Bound {
                start: a.max(c).clone(),
                end: b.clone(),
            },
            (Bound { start: a, end: b }, OpenLeft { end: d }) => Bound {
                start: a.clone(),
                end: b.min(d).clone(),
            },
            (OpenRight { start: a }, Bound { start: c, end: d }) => Bound {
                start: a.max(c).clone(),
                end: d.clone(),
            },
            (OpenRight { start: a }, OpenRight { start: c }) => OpenRight {
                start: a.max(c).clone(),
            },
            (OpenRight { start: a }, OpenLeft { end: d }) => Bound {
                start: a.clone(),
                end: d.clone(),
            },
            (OpenLeft { end: b }, Bound { start: c, end: d }) => Bound {
                start: c.clone(),
                end: b.min(d).clone(),
            },
            (OpenLeft { end: b }, OpenRight { start: c }) => Bound {
                start: c.clone(),
                end: b.clone(),
            },
            (OpenLeft { end: b }, OpenLeft { end: d }) => OpenLeft {
                end: b.min(d).clone(),
            },
        }
        .normalize()
    }

    /// Returns the union of the intervals.
    pub fn union(&self, other: &Self) -> IntegerInterval {
        use IntegerInterval::*;

        match (self, other) {
            (Empty, x) | (x, Empty) => x.clone(),
            (Open, _) | (_, Open) => Open,
            (Single(x), Single(y)) => Bound {
                start: x.min(y).clone(),
                end: x.max(y).clone(),
            },
            (Single(x), Bound { start: a, end: b })
            | (Bound { start: a, end: b }, Single(x)) => Bound {
                start: x.min(a).clone(),
                end: x.max(b).clone(),
            },
            (Single(x), OpenRight { start: a }) | (OpenRight { start: a }, Single(x)) => {
                OpenRight {
                    start: x.min(a).clone(),
                }
            }
            (Single(x), OpenLeft { end: b }) | (OpenLeft { end: b }, Single(x)) => {
                OpenLeft {
                    end: x.max(b).clone(),
                }
            }
            (Bound { start: a, end: b }, Bound { start: c, end: d }) => Bound {
                start: a.min(c).clone(),
                end: b.max(d).clone(),
            },
            (Bound { start: a, end: _ }, OpenRight { start: c })
            | (OpenRight { start: c }, Bound { start: a, end: _ }) => OpenRight {
                start: a.min(c).clone(),
            },
            (Bound { start: _, end: b }, OpenLeft { end: d })
            | (OpenLeft { end: d }, Bound { start: _, end: b }) => OpenLeft {
                end: b.max(d).clone(),
            },
            (OpenRight { start: a }, OpenRight { start: c }) => OpenRight {
                start: a.min(c).clone(),
            },
            (OpenRight { start: _ }, OpenLeft { end: _ })
            | (OpenLeft { .. }, OpenRight { .. }) => Open,
            (OpenLeft { end: b }, OpenLeft { end: d }) => OpenLeft {
                end: b.max(d).clone(),
            },
        }
        .normalize()
    }
}

impl std::fmt::Display for IntegerInterval {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            IntegerInterval::Empty => write!(f, "∅"),
            IntegerInterval::Single(x) => write!(f, "[{0},{0}]", x),
            IntegerInterval::Bound { start, end } => {
                write!(f, "[{},{}]", start, end)
            }
            IntegerInterval::OpenRight { start } => write!(f, "[{},∞]", start),
            IntegerInterval::OpenLeft { end } => write!(f, "[-∞,{}]", end),
            IntegerInterval::Open => write!(f, "[-∞,∞]"),
        }
    }
}

fn to_tuple_integer_interval(interval: &IntegerInterval) -> (Integer, Integer) {
    use Integer::*;
    use IntegerInterval::*;

    let f = |x: &BigInt| Finite(x.clone());
    let n = || NegativeInfinity;
    let p = || PositiveInfinity;

    match interval {
        Empty => unreachable!(),
        Single(x) => (f(x), f(x)),
        Bound { start, end } => (f(start), f(end)),
        OpenRight { start } => (f(start), p()),
        OpenLeft { end } => (n(), f(end)),
        Open => (n(), p()),
    }
}

#[cfg(test)]
mod test {
    pub use super::*;
    pub use proptest::prelude::*;

    pub fn interval_strategy() -> impl Strategy<Value = IntegerInterval> {
        prop_oneof![
            Just(IntegerInterval::Empty),
            Just(IntegerInterval::Open),
            any::<i8>().prop_map(|x| IntegerInterval::Single(x.into())),
            any::<i8>().prop_map(|x| IntegerInterval::OpenRight { start: x.into() }),
            any::<i8>().prop_map(|x| IntegerInterval::OpenLeft { end: x.into() }),
            any::<(i8, i8)>().prop_map(|(a, b)| IntegerInterval::new(a, b)),
        ]
    }

    #[track_caller]
    pub fn assert_in_interval(value: BigInt, interval: &IntegerInterval) {
        if !interval.contains(value.clone()) {
            panic!("{} does not contain test value: {}", interval, value);
        }
    }

    #[track_caller]
    pub fn assert_infinity_in_interval(interval: &IntegerInterval) {
        if !interval.contains_infinity() {
            panic!("{} does not contain test value: ∞", interval);
        }
    }

    #[track_caller]
    pub fn assert_neg_infinity_in_interval(interval: &IntegerInterval) {
        if !interval.contains_neg_infinity() {
            panic!("{} does not contain test value: -∞", interval);
        }
    }

    pub fn interval_into_integers(interval: IntegerInterval) -> (Integer, Integer) {
        let (s, e) = interval.into_options();
        (
            s.map(Integer::Finite).unwrap_or(Integer::NegativeInfinity),
            e.map(Integer::Finite).unwrap_or(Integer::PositiveInfinity),
        )
    }
}
