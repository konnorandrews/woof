use std::ops::Mul;

use crate::interval::integer::to_tuple_integer_interval;

use super::Integer;
use itertools::{Itertools, MinMaxResult};
use num_bigint::BigInt;

use super::IntegerInterval;

pub(super) fn min_max_to_interval<T>(arr: T) -> IntegerInterval
where
    T: IntoIterator,
    <T as IntoIterator>::Item: Into<Integer>,
{
    use Integer::*;
    use IntegerInterval::*;
    match arr.into_iter().map(|x| x.into()).minmax() {
        MinMaxResult::NoElements => unreachable!(),
        MinMaxResult::OneElement(Finite(x)) => Single(x),
        MinMaxResult::OneElement(NegativeInfinity) => Empty,
        MinMaxResult::OneElement(PositiveInfinity) => Empty,
        MinMaxResult::MinMax(Finite(start), Finite(end)) => Bound { start, end },
        MinMaxResult::MinMax(Finite(_), NegativeInfinity) => unreachable!(),
        MinMaxResult::MinMax(Finite(start), PositiveInfinity) => OpenRight { start },
        MinMaxResult::MinMax(NegativeInfinity, Finite(end)) => OpenLeft { end },
        MinMaxResult::MinMax(PositiveInfinity, Finite(_)) => unreachable!(),
        MinMaxResult::MinMax(PositiveInfinity, NegativeInfinity) => unreachable!(),
        MinMaxResult::MinMax(NegativeInfinity, PositiveInfinity) => Open,
        MinMaxResult::MinMax(NegativeInfinity, NegativeInfinity) => Empty,
        MinMaxResult::MinMax(PositiveInfinity, PositiveInfinity) => Empty,
    }
    .normalize()
}

impl<'a> Mul<&'a IntegerInterval> for &'a IntegerInterval {
    type Output = IntegerInterval;

    fn mul(self, rhs: Self) -> Self::Output {
        use IntegerInterval::*;

        if self == &Empty || rhs == &Empty {
            return Empty;
        }

        let (a, b) = to_tuple_integer_interval(self);
        let (c, d) = to_tuple_integer_interval(rhs);

        min_max_to_interval([&a * &c, &a * &d, &b * &c, &b * &d])
    }
}

#[test]
fn mul_examples() {
    // panic!("{}", &IntegerInterval::new(0, 0) * &IntegerInterval::OpenRight { start: 3.into() })
}

#[test]
fn mul() {
    use super::test::*;

    fn do_mul(a: IntegerInterval, b: IntegerInterval) {
        // Do operation under test to get interval.
        let c = &a * &b;

        let assert_in_c = |x, y| {
            use Integer::*;
            match (x, y) {
                (Finite(x), Finite(y)) => assert_in_interval(x * y, &c),
                (Finite(x), NegativeInfinity) => match x.cmp(&0.into()) {
                    Ordering::Less => assert_infinity_in_interval(&c),
                    Ordering::Equal => assert_in_interval(0.into(), &c),
                    Ordering::Greater => assert_neg_infinity_in_interval(&c),
                },
                (Finite(x), PositiveInfinity) => match x.cmp(&0.into()) {
                    Ordering::Less => assert_neg_infinity_in_interval(&c),
                    Ordering::Equal => assert_in_interval(0.into(), &c),
                    Ordering::Greater => assert_infinity_in_interval(&c),
                },
                (NegativeInfinity, Finite(y)) => match y.cmp(&0.into()) {
                    Ordering::Less => assert_infinity_in_interval(&c),
                    Ordering::Equal => assert_in_interval(0.into(), &c),
                    Ordering::Greater => assert_neg_infinity_in_interval(&c),
                },
                (PositiveInfinity, Finite(y)) => match y.cmp(&0.into()) {
                    Ordering::Less => assert_neg_infinity_in_interval(&c),
                    Ordering::Equal => assert_in_interval(0.into(), &c),
                    Ordering::Greater => assert_infinity_in_interval(&c),
                },
                (NegativeInfinity, NegativeInfinity) => assert_infinity_in_interval(&c),
                (PositiveInfinity, PositiveInfinity) => assert_infinity_in_interval(&c),
                (PositiveInfinity, NegativeInfinity) => {
                    assert_neg_infinity_in_interval(&c);
                }
                (NegativeInfinity, PositiveInfinity) => {
                    assert_neg_infinity_in_interval(&c);
                }
            }
        };

        if (matches!(&a, IntegerInterval::Empty) || matches!(&b, IntegerInterval::Empty))
        {
            assert!(matches!(c, IntegerInterval::Empty));
        } else {
            // low bound, low bound
            assert_in_c(
                interval_into_integers(a.clone()).0,
                interval_into_integers(b.clone()).0,
            );

            // low bound, high bound
            assert_in_c(
                interval_into_integers(a.clone()).0,
                interval_into_integers(b.clone()).1,
            );

            // high bound, low bound
            assert_in_c(
                interval_into_integers(a.clone()).1,
                interval_into_integers(b.clone()).0,
            );

            // high bound, high bound
            assert_in_c(
                interval_into_integers(a.clone()).1,
                interval_into_integers(b.clone()).1,
            );
        }
    }

    proptest!(|(a in interval_strategy(), b in interval_strategy())| {
        do_mul(a, b)
    });

    proptest!(|(a in interval_strategy(), b in interval_strategy())| {
        let c = &(&a * &b);
        let d = &(&b * &a);
        assert_eq!(c, d);
    });

    proptest!(|(a in interval_strategy(), b in interval_strategy())| {
        let c = &(&a * &b) / &b;
        if a == IntegerInterval::Empty || b == IntegerInterval::Empty {
            assert_eq!(c, IntegerInterval::Empty)
        } else if b.contains(0) {
            assert_eq!(c, IntegerInterval::Empty)
        } else {
            if !a.is_subinterval_of(&c) {
                panic!("{} is not a subinterval of {}", a, c);
            }
        }
    });

    proptest!(|(a in interval_strategy(), b in interval_strategy())| {
        let c = &(&a * &b) / &a;
        if a == IntegerInterval::Empty || b == IntegerInterval::Empty {
            assert_eq!(c, IntegerInterval::Empty)
        } else if a.contains(0) {
            assert_eq!(c, IntegerInterval::Empty)
        } else {
            if !b.is_subinterval_of(&c) {
                panic!("{} is not a subinterval of {}", b, c);
            }
        }
    });
}
