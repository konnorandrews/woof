use std::ops::Neg;

use super::IntegerInterval;

impl<'a> Neg for &'a IntegerInterval {
    type Output = IntegerInterval;

    fn neg(self) -> Self::Output {
        &IntegerInterval::Single(0.into()) - self
    }
}

#[test]
fn neg() {
    use super::test::*;
    fn do_neg(a: IntegerInterval) {
        // Do operation under test to get interval.
        let c = -&a;

        let assert_in_c = |x| {
            use Integer::*;
            match x {
                NegativeInfinity => assert_infinity_in_interval(&c),
                Finite(x) => assert_in_interval(-x, &c),
                PositiveInfinity => assert_neg_infinity_in_interval(&c),
            }
        };

        if matches!(&a, IntegerInterval::Empty) {
            assert!(matches!(c, IntegerInterval::Empty));
        } else {
            // low bound
            assert_in_c(interval_into_integers(a.clone()).0);

            // high bound
            assert_in_c(interval_into_integers(a.clone()).1);
        }
    }

    proptest!(|(a in interval_strategy())| do_neg(a));
}
