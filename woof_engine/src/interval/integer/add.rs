use std::ops::Add;

use crate::interval::integer::to_tuple_integer_interval;

use super::{Integer, IntegerInterval};

pub(super) fn add_to_interval(
    a: Integer,
    b: Integer,
    c: Integer,
    d: Integer,
) -> IntegerInterval {
    use Integer::*;
    use IntegerInterval::*;

    let add_integers = |x, y| match (x, y) {
        (NegativeInfinity, NegativeInfinity) => NegativeInfinity,
        (NegativeInfinity, Finite(_)) => NegativeInfinity,
        (NegativeInfinity, PositiveInfinity) => unreachable!(),
        (Finite(_), NegativeInfinity) => NegativeInfinity,
        (Finite(x), Finite(y)) => Finite(x + y),
        (Finite(_), PositiveInfinity) => PositiveInfinity,
        (PositiveInfinity, NegativeInfinity) => unreachable!(),
        (PositiveInfinity, Finite(_)) => PositiveInfinity,
        (PositiveInfinity, PositiveInfinity) => PositiveInfinity,
    };

    let start = add_integers(a, c);
    let end = add_integers(b, d);

    match (start, end) {
        (NegativeInfinity, NegativeInfinity) => unreachable!(),
        (NegativeInfinity, Finite(end)) => OpenLeft { end },
        (NegativeInfinity, PositiveInfinity) => Open,
        (Finite(_), NegativeInfinity) => unreachable!(),
        (Finite(start), Finite(end)) => Bound { start, end },
        (Finite(start), PositiveInfinity) => OpenRight { start },
        (PositiveInfinity, NegativeInfinity) => unreachable!(),
        (PositiveInfinity, Finite(_)) => unreachable!(),
        (PositiveInfinity, PositiveInfinity) => unreachable!(),
    }
    .normalize()
}

impl<'a> Add<&'a IntegerInterval> for &'a IntegerInterval {
    type Output = IntegerInterval;

    fn add(self, rhs: Self) -> Self::Output {
        use IntegerInterval::*;

        if self == &Empty || rhs == &Empty {
            return Empty;
        }

        let (a, b) = to_tuple_integer_interval(self);
        let (c, d) = to_tuple_integer_interval(rhs);

        add_to_interval(a, b, c, d)
    }
}

#[test]
fn add() {
    use super::test::*;

    fn do_add(a: IntegerInterval, b: IntegerInterval) {
        // Do operation under test to get interval.
        let c = &a + &b;

        let assert_in_c = |x, y| {
            use Integer::*;
            match (x, y) {
                (Finite(x), Finite(y)) => assert_in_interval(x + y, &c),
                (Finite(_), NegativeInfinity) => assert_neg_infinity_in_interval(&c),
                (Finite(_), PositiveInfinity) => assert_infinity_in_interval(&c),
                (NegativeInfinity, Finite(_)) => assert_neg_infinity_in_interval(&c),
                (PositiveInfinity, Finite(_)) => assert_infinity_in_interval(&c),
                (NegativeInfinity, NegativeInfinity) => {
                    assert_neg_infinity_in_interval(&c)
                }
                (PositiveInfinity, PositiveInfinity) => assert_infinity_in_interval(&c),
                (PositiveInfinity, NegativeInfinity) => {
                    assert_neg_infinity_in_interval(&c);
                    assert_infinity_in_interval(&c);
                }
                (NegativeInfinity, PositiveInfinity) => {
                    assert_neg_infinity_in_interval(&c);
                    assert_infinity_in_interval(&c);
                }
            }
        };

        if (matches!(&a, IntegerInterval::Empty) || matches!(&b, IntegerInterval::Empty))
        {
            assert!(matches!(c, IntegerInterval::Empty));
        } else {
            // low bound, low bound
            assert_in_c(
                interval_into_integers(a.clone()).0,
                interval_into_integers(b.clone()).0,
            );

            // low bound, high bound
            assert_in_c(
                interval_into_integers(a.clone()).0,
                interval_into_integers(b.clone()).1,
            );

            // high bound, low bound
            assert_in_c(
                interval_into_integers(a.clone()).1,
                interval_into_integers(b.clone()).0,
            );

            // high bound, high bound
            assert_in_c(
                interval_into_integers(a.clone()).1,
                interval_into_integers(b.clone()).1,
            );
        }
    }

    proptest!(|(a in interval_strategy(), b in interval_strategy())| {
        do_add(a, b)
    });

    proptest!(|(a in interval_strategy(), b in interval_strategy())| {
        let c = &(&a + &b);
        let d = &(&b + &a);
        assert_eq!(c, d);
    });

    proptest!(|(a in interval_strategy(), b in interval_strategy())| {
        let c = &(&(&a + &b) - &b);
        let d = &(&(&a + &b) - &a);

        if a == IntegerInterval::Empty || b == IntegerInterval::Empty {
            assert_eq!(c, &IntegerInterval::Empty);
            assert_eq!(d, &IntegerInterval::Empty);
        } else {
            assert!(a.is_subinterval_of(&c));
            assert!(b.is_subinterval_of(&d));
        }
    });
}
