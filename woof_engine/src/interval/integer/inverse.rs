use num_bigint::BigInt;
use num_rational::Ratio;

use crate::interval::rational::RationalInterval;

use super::IntegerInterval;

impl IntegerInterval {
    pub(super) fn inverse(mut self) -> RationalInterval {
        self = self.normalize();

        if matches!(&self, IntegerInterval::Empty) {
            return RationalInterval::Empty;
        }

        if let IntegerInterval::Single(x) = &self {
            if x == &BigInt::from(0) {
                return RationalInterval::Empty;
            }
        }

        if !self.contains(0) {
            match self {
                IntegerInterval::Empty => unreachable!(),
                IntegerInterval::Single(x) => {
                    RationalInterval::Single(Ratio::from_integer(x.clone()).recip())
                }
                IntegerInterval::Bound { start, end } => RationalInterval::Bound {
                    start: Ratio::from_integer(end.clone()).recip(),
                    end: Ratio::from_integer(start.clone()).recip(),
                },
                IntegerInterval::OpenRight { start } => RationalInterval::Bound {
                    start: Ratio::from_integer(0.into()),
                    end: Ratio::from_integer(start.clone()).recip(),
                },
                IntegerInterval::OpenLeft { end } => RationalInterval::Bound {
                    start: Ratio::from_integer(end.clone()).recip(),
                    end: Ratio::from_integer(0.into()),
                },
                IntegerInterval::Open => {
                    RationalInterval::Single(Ratio::from_integer(0.into()))
                }
            }
        } else {
            let (start, end) = self.into_options();

            let start_less_zero = start
                .as_ref()
                .map(|start| start < &BigInt::from(0))
                .unwrap_or(true);

            let zero_less_end = end
                .as_ref()
                .map(|end| &BigInt::from(0) < end)
                .unwrap_or(true);

            let start_eq_zero = start
                .as_ref()
                .map(|start| &BigInt::from(0) == start)
                .unwrap_or(false);

            let zero_neq_end = end
                .as_ref()
                .map(|end| &BigInt::from(0) != end)
                .unwrap_or(true);

            let start_neq_zero = !start_eq_zero;

            let zero_eq_end = !zero_neq_end;

            if start_less_zero && zero_less_end {
                // RationalInterval::Open
                RationalInterval::Bound {
                    start: Ratio::from_integer((-1).into()),
                    end: Ratio::from_integer(1.into()),
                }
            } else if start_eq_zero && zero_neq_end {
                match end {
                    // Some(x) => RationalInterval::OpenRight {
                    //     start: Ratio::from_integer(x).recip(),
                    // },
                    // None => RationalInterval::OpenRight {
                    //     start: Ratio::from_integer(0.into()),
                    // },
                    Some(x) => RationalInterval::Bound {
                        start: Ratio::from_integer(x).recip(),
                        end: Ratio::from_integer(1.into()),
                    },
                    None => RationalInterval::Bound {
                        start: Ratio::from_integer(0.into()),
                        end: Ratio::from_integer(1.into()),
                    },
                }
            } else if start_neq_zero && zero_eq_end {
                match start {
                    // Some(x) => RationalInterval::OpenLeft {
                    //     end: Ratio::from_integer(x).recip(),
                    // },
                    // None => RationalInterval::OpenLeft {
                    //     end: Ratio::from_integer(0.into()),
                    // },
                    Some(x) => RationalInterval::Bound {
                        start: Ratio::from_integer((-1).into()),
                        end: Ratio::from_integer(x).recip(),
                    },
                    None => RationalInterval::Bound {
                        start: Ratio::from_integer((-1).into()),
                        end: Ratio::from_integer(0.into()),
                    },
                }
            } else {
                unreachable!(
                    "{:?}..{:?}, s<0: {}, 0<e: {}, s=0: {}, 0!=e: {}, s!=0: {}, e=0: {}",
                    start,
                    end,
                    start_less_zero,
                    zero_less_end,
                    start_eq_zero,
                    zero_neq_end,
                    start_neq_zero,
                    zero_eq_end
                )
            }
        }
    }
}
