use std::ops::Div;

use itertools::{Itertools, MinMaxResult};
use num_bigint::BigInt;
use num_rational::Ratio;

use crate::interval::integer::{to_tuple_integer_interval, Integer};
use crate::interval::rational::{to_tuple_rational_interval, Rational, RationalInterval};

use super::mul::min_max_to_interval;
use super::IntegerInterval;

impl<'a> Div<&'a IntegerInterval> for &'a IntegerInterval {
    type Output = IntegerInterval;

    fn div(self, rhs: Self) -> Self::Output {
        use IntegerInterval::*;

        fn mul(lhs: &IntegerInterval, rhs: &RationalInterval) -> IntegerInterval {
            if lhs == &Empty || rhs == &RationalInterval::Empty {
                return Empty;
            }

            let (a, b) = to_tuple_integer_interval(lhs);
            let (c, d) = to_tuple_rational_interval(rhs);

            min_max_to_interval([&a * &c, &a * &d, &b * &c, &b * &d])
        }

        if rhs.contains(0) {
            IntegerInterval::Empty
        } else {
            mul(dbg!(self), dbg!(&rhs.clone().inverse()))
        }
    }
}

#[test]
fn div() {
    use super::test::*;

    fn do_div(a: IntegerInterval, b: IntegerInterval) {
        // Do operation under test to get interval.
        let c = &a / &b;

        let assert_in_c = |x, y| {
            use Integer::*;
            match (x, y) {
                (Finite(x), Finite(y)) => assert_in_interval(x / y, &c),
                (Finite(_), NegativeInfinity) => {
                    assert_in_interval(0.into(), &c);
                }
                (Finite(_), PositiveInfinity) => {
                    assert_in_interval(0.into(), &c);
                }
                (NegativeInfinity, Finite(y)) => match y.cmp(&0.into()) {
                    Ordering::Less => assert_infinity_in_interval(&c),
                    Ordering::Equal => unreachable!(),
                    Ordering::Greater => assert_neg_infinity_in_interval(&c),
                },
                (PositiveInfinity, Finite(y)) => match y.cmp(&0.into()) {
                    Ordering::Less => assert_neg_infinity_in_interval(&c),
                    Ordering::Equal => unreachable!(),
                    Ordering::Greater => assert_infinity_in_interval(&c),
                },
                (NegativeInfinity, NegativeInfinity) => {
                    assert_infinity_in_interval(&c);
                    assert_in_interval(0.into(), &c);
                }
                (PositiveInfinity, PositiveInfinity) => {
                    assert_infinity_in_interval(&c);
                    assert_in_interval(0.into(), &c);
                }
                (PositiveInfinity, NegativeInfinity) => {
                    assert_neg_infinity_in_interval(&c);
                    assert_in_interval(0.into(), &c);
                }
                (NegativeInfinity, PositiveInfinity) => {
                    assert_neg_infinity_in_interval(&c);
                    assert_in_interval(0.into(), &c);
                }
            }
        };

        if (matches!(&a, IntegerInterval::Empty) || matches!(&b, IntegerInterval::Empty))
        {
            assert!(matches!(c, IntegerInterval::Empty));
        } else if b.contains(0) {
            assert!(matches!(c, IntegerInterval::Empty));
        } else if a == IntegerInterval::Single(0.into()) {
            assert_eq!(c, IntegerInterval::Single(0.into()));
        } else {
            // low bound, low bound
            assert_in_c(
                interval_into_integers(a.clone()).0,
                interval_into_integers(b.clone()).0,
            );

            // low bound, high bound
            assert_in_c(
                interval_into_integers(a.clone()).0,
                interval_into_integers(b.clone()).1,
            );

            // high bound, low bound
            assert_in_c(
                interval_into_integers(a.clone()).1,
                interval_into_integers(b.clone()).0,
            );

            // high bound, high bound
            assert_in_c(
                interval_into_integers(a.clone()).1,
                interval_into_integers(b.clone()).1,
            );
        }
    }

    proptest!(|(a in interval_strategy(), b in interval_strategy())| {
        do_div(a, b)
    });
}
