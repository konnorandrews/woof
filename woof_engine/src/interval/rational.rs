use std::{cmp::Ordering, ops::Mul};

use num_bigint::BigInt;
use num_rational::Ratio;

use super::integer::Integer;

// Inclusive integer interval arithmetic.
#[derive(PartialEq, Clone, Eq, Debug)]
pub enum RationalInterval {
    Empty,
    Single(Ratio<BigInt>),
    Bound {
        start: Ratio<BigInt>,
        end: Ratio<BigInt>,
    },
    OpenRight {
        start: Ratio<BigInt>,
    },
    OpenLeft {
        end: Ratio<BigInt>,
    },
    Open,
}

#[derive(PartialEq, Clone, Eq, Debug)]
pub enum Rational {
    NegativeInfinity,
    Finite(Ratio<BigInt>),
    PositiveInfinity,
}

impl From<Ratio<BigInt>> for Rational {
    fn from(value: Ratio<BigInt>) -> Self {
        Rational::Finite(value)
    }
}

impl Mul for &Rational {
    type Output = Rational;

    fn mul(self, rhs: Self) -> Self::Output {
        let zero = Ratio::from_integer(0.into());

        use Rational::*;
        match (self, rhs) {
            (NegativeInfinity, NegativeInfinity) => PositiveInfinity,
            (NegativeInfinity, Finite(y)) => match y.cmp(&zero) {
                Ordering::Less => PositiveInfinity,
                Ordering::Equal => Finite(zero),
                Ordering::Greater => NegativeInfinity,
            },
            (NegativeInfinity, PositiveInfinity) => NegativeInfinity,
            (Finite(x), NegativeInfinity) => match x.cmp(&zero) {
                Ordering::Less => PositiveInfinity,
                Ordering::Equal => Finite(zero),
                Ordering::Greater => NegativeInfinity,
            },
            (Finite(x), Finite(y)) => Finite(x * y),
            (Finite(x), PositiveInfinity) => match x.cmp(&zero) {
                Ordering::Less => NegativeInfinity,
                Ordering::Equal => Finite(zero),
                Ordering::Greater => PositiveInfinity,
            },
            (PositiveInfinity, NegativeInfinity) => NegativeInfinity,
            (PositiveInfinity, Finite(y)) => match y.cmp(&zero) {
                Ordering::Less => NegativeInfinity,
                Ordering::Equal => Finite(zero),
                Ordering::Greater => PositiveInfinity,
            },
            (PositiveInfinity, PositiveInfinity) => PositiveInfinity,
        }
    }
}

impl Mul<&Rational> for &Integer {
    type Output = Integer;

    fn mul(self, rhs: &Rational) -> Self::Output {
        let zero = BigInt::from(0);
        let r_zero = Ratio::from_integer(BigInt::from(0));

        use Integer::*;
        match (self, rhs) {
            (NegativeInfinity, Rational::NegativeInfinity) => PositiveInfinity,
            (NegativeInfinity, Rational::Finite(y)) => match y.cmp(&r_zero) {
                Ordering::Less => PositiveInfinity,
                Ordering::Equal => Finite(zero),
                Ordering::Greater => NegativeInfinity,
            },
            (NegativeInfinity, Rational::PositiveInfinity) => NegativeInfinity,
            (Finite(x), Rational::NegativeInfinity) => match x.cmp(&zero) {
                Ordering::Less => PositiveInfinity,
                Ordering::Equal => Finite(zero),
                Ordering::Greater => NegativeInfinity,
            },
            (Finite(x), Rational::Finite(y)) => {
                let c: Ratio<BigInt> = Ratio::from_integer(x.clone()) * y;
                Finite(c.to_integer())
            }
            (Finite(x), Rational::PositiveInfinity) => match x.cmp(&zero) {
                Ordering::Less => NegativeInfinity,
                Ordering::Equal => Finite(zero),
                Ordering::Greater => PositiveInfinity,
            },
            (PositiveInfinity, Rational::NegativeInfinity) => NegativeInfinity,
            (PositiveInfinity, Rational::Finite(y)) => match y.cmp(&r_zero) {
                Ordering::Less => NegativeInfinity,
                Ordering::Equal => Finite(zero),
                Ordering::Greater => PositiveInfinity,
            },
            (PositiveInfinity, Rational::PositiveInfinity) => PositiveInfinity,
        }
    }
}

impl Ord for Rational {
    fn cmp(&self, other: &Self) -> Ordering {
        use Rational::*;
        match (self, other) {
            (NegativeInfinity, NegativeInfinity) => Ordering::Equal,
            (NegativeInfinity, Finite(_)) => Ordering::Less,
            (NegativeInfinity, PositiveInfinity) => Ordering::Less,
            (Finite(_), NegativeInfinity) => Ordering::Greater,
            (Finite(x), Finite(y)) => x.cmp(y),
            (Finite(_), PositiveInfinity) => Ordering::Less,
            (PositiveInfinity, NegativeInfinity) => Ordering::Greater,
            (PositiveInfinity, Finite(_)) => Ordering::Greater,
            (PositiveInfinity, PositiveInfinity) => Ordering::Equal,
        }
    }
}

impl PartialOrd for Rational {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

pub(super) fn to_tuple_rational_interval(
    interval: &RationalInterval,
) -> (Rational, Rational) {
    use Rational::*;
    use RationalInterval::*;

    let f = |x: &Ratio<BigInt>| Finite(x.clone());
    let n = || NegativeInfinity;
    let p = || PositiveInfinity;

    match interval {
        Empty => unreachable!(),
        Single(x) => (f(x), f(x)),
        Bound { start, end } => (f(start), f(end)),
        OpenRight { start } => (f(start), p()),
        OpenLeft { end } => (n(), f(end)),
        Open => (n(), p()),
    }
}
