pub mod ident_stack;
pub mod integer;

use std::{
    collections::{HashMap, HashSet},
    ops::Deref,
};

use num_bigint::BigInt;

use crate::interval::IntegerInterval;

use self::{
    ident_stack::IdentifierStack,
    integer::{Integer, IntegerSign, IntegerWidth, TargetPointerWidth},
};

pub type Error = &'static str;

pub enum Loop {
    While(While),
    Iterate(Iterate),
}

#[derive(Debug)]
pub struct Block {
    nodes: Vec<Node>,
}

impl Block {
    pub fn new(nodes: Vec<Node>) -> Self {
        Self { nodes }
    }

    pub fn into_expression(
        self,
        pointer_width: TargetPointerWidth,
    ) -> Result<Expression, Error> {
        let mut local_idents = HashSet::<Identifier>::new();

        let mut has_internal_state = false;
        let mut has_await = false;
        let mut breaks = HashMap::new();
        let mut continues = HashSet::new();
        let mut return_type = None;
        let mut pure_inputs = HashSet::new();
        let mut in_out = HashSet::new();

        for node in &self.nodes {
            use Node::*;
            match node {
                Argument(_) => todo!(),
                Prove(_) => todo!(),
                Assign(_) => todo!(),
                Constant(_) => todo!(),
                Expression(expr) => {
                    has_internal_state |= expr.has_internal_state;
                    has_await |= expr.has_await;
                    breaks
                        .extend(expr.breaks.iter().map(|(a, b)| (a.clone(), b.clone())));
                    continues.extend(expr.continues.iter().cloned());

                    if let (Some(a), Some(b)) = (&mut return_type, &expr.return_type) {
                        let mut a = a;
                        if let (Type::Integer(a), Type::Integer(b)) = (&mut a, b) {
                            if a.is_same_base(b) {
                                *a = Integer::new_refined(
                                    a.width(),
                                    a.signed(),
                                    pointer_width,
                                    a.interval().union(b.interval()),
                                )?;
                            } else {
                                return Err("returns don't have the same integer type");
                            }
                        } else if a != b {
                            return Err("returns don't have the same type");
                        }
                    } else if let Some(x) = &expr.return_type {
                        return_type = Some(x.clone());
                    }

                    pure_inputs.extend(
                        expr.pure_inputs
                            .iter()
                            .filter(|ident| !local_idents.contains(ident)),
                    );
                    in_out.extend(
                        expr.in_out
                            .iter()
                            .filter(|ident| !local_idents.contains(ident)),
                    );
                }
                Field(_) => todo!(),
                Function(_) => todo!(),
                If(_) => todo!(),
                Iterate(_) => todo!(),
                Jump(_) => todo!(),
                Return(_) => todo!(),
                Status(_) => todo!(),
                Struct(_) => todo!(),
                TypeExpression(_) => todo!(),
                Variable(_) => todo!(),
                While(_) => todo!(),
            }
        }

        pure_inputs.retain(|ident| !in_out.contains(ident));

        let ty = self
            .nodes
            .last()
            .map(|node| match node {
                Node::Expression(expr) => expr.r#type.clone(),
                _ => Type::Tuple(Vec::new()),
            })
            .unwrap_or_else(|| Type::Tuple(Vec::new()));

        Ok(Expression {
            r#type: ty,
            kind: ExpressionKind::Block(self),
            has_internal_state,
            has_await,
            breaks,
            continues,
            return_type,
            pure_inputs,
            in_out,
        })
    }
}

#[derive(Debug)]
pub enum Node {
    Argument(Argument),
    Prove(Prove),
    Assign(Assign),
    // Choose(Choose),
    Constant(Constant),
    Expression(Expression),
    Field(Field),
    Function(Function),
    If(If),
    Iterate(Iterate),
    Jump(Jump),
    Return(Return),
    Status(Status),
    Struct(Struct),
    TypeExpression(TypeExpression),
    // Use(Use),
    Variable(Variable),
    While(While),
}

#[derive(Debug)]
pub struct Argument {}
#[derive(Debug)]
pub struct Prove {}
#[derive(Debug)]
pub struct Assign {}
#[derive(Debug)]
pub struct Constant {}

/// An expression
#[derive(Debug)]
pub struct Expression {
    /// The type of the value the expression evaluates to.
    r#type: Type,
    /// The kind of expression.
    kind: ExpressionKind,
    /// Flag for if the expression has internal state.
    /// Internal state allows the expression to return different values
    /// for the exact same `pure_inputs` and `in_out` values.
    has_internal_state: bool,
    /// Flag for if the expression contains a `.await` expression;
    has_await: bool,
    /// Flag for if the expression can break to an outside control flow.
    breaks: HashMap<Label, Type>,
    /// Flag for if the expression contains a `continue` statement.
    continues: HashSet<Label>,
    /// Flag for if the expression can resturn and error from the surrounding function.
    return_type: Option<Type>,
    /// Outside identifiers the expression will read when evaluated.
    /// The use of these identifiers will not cause side effects.
    pure_inputs: HashSet<Identifier>,
    /// Outside identifiers the expression will use when evaluated.
    /// The use of these identifiers may cause side effects because of mutation.
    in_out: HashSet<Identifier>,
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
pub struct Label(u32);

impl Label {
    pub const NEAREST: Self = Self(0);

    pub fn new(value: u32) -> Self {
        Self(value)
    }
}

impl Expression {
    pub fn new(
        kind: ExpressionKind,
        ident_type_stack: &IdentifierStack<IdentType>,
        pointer_width: TargetPointerWidth,
    ) -> Result<Self, Error> {
        use ExpressionKind::*;

        Ok(match kind {
            GlobalIdentifier(ident) => Self {
                r#type: ident_type_stack
                    .get(&ident)
                    .ok_or("failed to find global identifier in member type map")?
                    .0
                    .to_value_type(),
                kind,
                has_internal_state: false,
                has_await: false,
                pure_inputs: [ident].into_iter().collect(), // By itself a identifier expression is pure.
                in_out: HashSet::new(),
                breaks: HashMap::new(),
                continues: HashSet::new(),
                return_type: None,
            },
            Identifier(ident) => Self {
                r#type: ident_type_stack
                    .get(&ident)
                    .ok_or("failed to find identifier in member type map")?
                    .0
                    .to_value_type(),
                kind,
                has_internal_state: false,
                has_await: false,
                pure_inputs: [ident].into_iter().collect(), // By itself a identifier expression is pure.
                in_out: HashSet::new(),
                breaks: HashMap::new(),
                continues: HashSet::new(),
                return_type: None,
            },
            Member(ident) => Self {
                r#type: Type::Never,
                kind,
                has_internal_state: false,
                has_await: false,
                pure_inputs: [ident].into_iter().collect(), // By itself a member expression is pure.
                in_out: HashSet::new(),
                breaks: HashMap::new(),
                continues: HashSet::new(),
                return_type: None,
            },
            ConstantValue(value) => Self {
                r#type: value.expression_type(pointer_width)?,
                kind: ConstantValue(value),
                has_internal_state: false,
                has_await: false,
                pure_inputs: HashSet::new(),
                in_out: HashSet::new(),
                breaks: HashMap::new(),
                continues: HashSet::new(),
                return_type: None,
            },
            BinaryExpression(expr) => {
                expr.into_expression(ident_type_stack, pointer_width)?
            }
            UnaryExpression(expr) => todo!(),
            FollowingUnaryExpression(_) => todo!(),
            Operation(_) => todo!(),
            List(_) => todo!(),
            Block(block) => block.into_expression(pointer_width)?,
        })
    }

    fn normalize_inputs(mut self) -> Self {
        self.pure_inputs
            .retain(|ident| !self.in_out.contains(ident));
        self
    }

    pub fn r#type(&self) -> &Type {
        &self.r#type
    }
}

#[derive(Copy, Clone, Debug)]
pub enum Effect {
    /// Expression does not change any observable state outside the expression.
    ///
    /// This means a pure expression can be repeated any
    /// number of times and result in the same result.
    Pure,
    /// Expression that does change some observable state outside the expression.
    Impure,
    /// A coroutine.
    ///
    /// A coroutine is always impure because it yields control to the caller.
    ImpureCoroutine,
}

impl Effect {
    pub fn include(&mut self, effect: Effect) {
        use Effect::*;

        match (&self, effect) {
            (Pure, Impure) => *self = Effect::Impure,
            (Pure, ImpureCoroutine) | (Impure, ImpureCoroutine) => {
                *self = Effect::ImpureCoroutine
            }
            _ => {}
        }
    }

    pub fn combine(&self, other: &Self) -> Self {
        use Effect::*;

        match (self, other) {
            (Pure, Pure) => Pure,
            (Pure, Impure) => Impure,
            (Pure, ImpureCoroutine) => ImpureCoroutine,
            (Impure, Pure) => Impure,
            (Impure, Impure) => Impure,
            (Impure, ImpureCoroutine) => ImpureCoroutine,
            (ImpureCoroutine, Pure) => ImpureCoroutine,
            (ImpureCoroutine, Impure) => ImpureCoroutine,
            (ImpureCoroutine, ImpureCoroutine) => ImpureCoroutine,
        }
    }
}

#[derive(Clone, PartialEq, Debug)]
pub enum IdentType {
    ValueType(Type),
    Module(HashMap<Identifier, IdentType>),
    Struct(HashMap<Identifier, IdentType>),
    Enum(HashMap<Identifier, IdentType>),
}

impl IdentType {
    pub fn to_value_type(&self) -> Type {
        match self {
            IdentType::ValueType(ty) => ty.clone(),
            IdentType::Module(_) => Type::Never,
            IdentType::Struct(_) => Type::Never,
            IdentType::Enum(_) => Type::Never,
        }
    }
}

#[derive(Clone, PartialEq, Debug)]
pub enum Type {
    /// `true` or `false`
    Bool,
    /// `(-∞, ∞)`
    Integer(Integer),
    /// `"foo bar"`
    Status,
    /// `&foo`
    Borrow(Mutability, Box<Type>),
    /// `&foo as _`
    Pointer(Mutability, Box<Type>),
    /// Pointer that can't be null.
    NonNullPointer(Mutability, Box<Type>),
    /// A tuple of values with possibly different types.
    Tuple(Vec<Type>),
    /// A array of values with a given length.
    Array(Box<Type>, usize),
    IoReader,
    IoWriter,
    Function(Vec<Type>, Box<Type>, bool),
    Method(Box<Type>, This, Vec<Type>, Box<Type>, bool),
    Struct(Identifier),
    Enum(Identifier),
    /// `!`
    Never,
    Range(
        IntegerWidth,
        IntegerSign,
        IntegerInterval,
        IntegerInterval,
        RangeKind,
    ),
}

#[derive(Clone, PartialEq, Copy, Debug)]
pub enum RangeKind {
    RightExclusive,
    RightInclusive,
}

#[derive(Clone, PartialEq, Copy, Debug)]
pub enum This {
    Owned,
    Borrowed(Mutability),
}

impl This {
    pub fn allowed_by(&self, other: &Self) -> bool {
        use Mutability::*;
        use This::*;
        match (self, other) {
            (Borrowed(Immutable), _) => true,
            (Borrowed(Mutable), Borrowed(Mutable)) => true,
            (Borrowed(Mutable), Owned) => true,
            (Owned, Owned) => true,
            _ => false,
        }
    }
}

impl Type {
    pub fn as_callable(&self) -> Option<Result<(&[Type], &Type, bool), Error>> {
        use Mutability::*;
        use This::*;
        use Type::*;

        let mut mutability = This::Owned;
        let mut ty = self;
        loop {
            match ty {
                Borrow(inner_mutability, inner_ty) => {
                    match (&mutability, inner_mutability) {
                        (Owned, Immutable) => {
                            mutability = This::Borrowed(Mutability::Immutable);
                        }
                        (Owned, Mutable) => {
                            mutability = This::Borrowed(Mutability::Mutable);
                        }
                        (Borrowed(Mutable), Immutable) => {
                            mutability = This::Borrowed(Mutability::Immutable);
                        }
                        _ => {}
                    }
                    ty = inner_ty;
                }
                Function(args, ret, has_internal_state) => {
                    return Some(Ok((args, ret, *has_internal_state)))
                }
                Method(_, this, args, ret, has_internal_state) => {
                    if this.allowed_by(&mutability) {
                        return Some(Ok((args, ret, *has_internal_state)));
                    } else {
                        return Some(Err(
                            "attempted to call method that requires a stronger ownership",
                        ));
                    }
                }
                _ => return None,
            }
        }
    }
}

#[derive(Clone, PartialEq, Copy, Debug)]
pub enum Mutability {
    Immutable,
    Mutable,
}

#[derive(Debug)]
pub enum ExpressionKind {
    /// Identifier of a global value.
    GlobalIdentifier(Identifier),
    /// Identifier of a local value.
    Identifier(Identifier),
    /// Identifier for a member of a value.
    Member(Identifier),
    /// Constant literal.
    ConstantValue(ConstantValue),
    /// Binary expression of the form `lhs op rhs`.
    BinaryExpression(Box<BinaryExpression>),
    /// Unary expression of the form `op expr`.
    UnaryExpression(Box<UnaryExpression>),
    /// Unary expression of the form `expr op`.
    FollowingUnaryExpression(Box<FollowingUnaryExpression>),
    /// Operator without expression.
    Operation(Operation),
    /// List of the form `expr1, expr2, expr3, ...`.
    List(Vec<Expression>),
    /// A block of statements `{ ... }`.
    Block(Block),
}

#[derive(Debug)]
pub enum ConstantValue {
    /// `true` or `false`
    Bool(bool),
    /// `(-∞, ∞)`
    Integer(BigInt, IntegerWidth, IntegerSign),
    /// `"foo bar"`
    Status(String),
    /// `[foo, bar]`
    Array(Vec<Expression>),
}

impl ConstantValue {
    pub fn expression_type(
        &self,
        pointer_width: TargetPointerWidth,
    ) -> Result<Type, Error> {
        use ConstantValue::*;

        Ok(match self {
            Bool(_) => Type::Bool,
            Integer(value, width, signed) => {
                Type::Integer(self::integer::Integer::new_refined(
                    *width,
                    *signed,
                    pointer_width,
                    IntegerInterval::Single(value.clone()),
                )?)
            }
            Status(_) => Type::Status,
            Array(exprs) => {
                let mut element_ty = exprs
                    .first()
                    .ok_or("expected at least one element in array")?
                    .r#type
                    .clone();
                for expr in exprs.iter().skip(1) {
                    match (&mut element_ty, &expr.r#type) {
                        (Type::Integer(a), Type::Integer(b)) => {
                            if a.is_same_base(b) {
                                // Make the refinement of the array type include all
                                // element refinements.
                                *a = integer::Integer::new_refined(
                                    a.width(),
                                    b.signed(),
                                    pointer_width,
                                    a.interval().union(b.interval()),
                                )?;
                            } else {
                                return Err("integer types in array don't have the same base type");
                            }
                        }
                        (a, b) if a == b => {}
                        (_, _) => {
                            return Err("array elements do not have compatible types")
                        }
                    }
                }
                Type::Array(Box::new(element_ty), exprs.len())
            }
        })
    }
}

/// ID of a identifier like `foo`.
#[derive(Clone, PartialEq, Eq, Hash, Copy, Debug)]
pub struct Identifier(u32);

impl Identifier {
    pub fn new(value: u32) -> Self {
        Self(value)
    }
}

/// `lhs op rhs`
#[derive(Debug)]
pub struct BinaryExpression {
    pub lhs: Expression,
    pub rhs: Expression,
    pub operation: BinaryOperation,
}

impl BinaryExpression {
    pub fn into_expression(
        self,
        ident_type_stack: &IdentifierStack<IdentType>,
        pointer_width: TargetPointerWidth,
    ) -> Result<Expression, Error> {
        use BinaryOperation::*;
        use Type::*;
        match &self.operation {
            Call => {
                if let Some(callable) = self.lhs.r#type.as_callable() {
                    let (args, ret, has_internal_state) = callable?;
                    match args.len() {
                        0 => return Err("did not expect arguments to function call"),
                        1 => {
                            if args[1] != self.rhs.r#type {
                                return Err("function argument type does not match");
                            }
                        }
                        _ => {
                            if let Tuple(types) = &self.rhs.r#type {
                                if args != types {
                                    return Err("function argument types do not match");
                                }
                            } else {
                                return Err("function arguments are not a tuple");
                            }
                        }
                    }
                    let ty = ret.clone();
                    self.make_expression(ty, has_internal_state)
                } else {
                    Err("expected a callable for lhs")
                }
            }
            Index => {
                if let Array(ty, len) = &self.lhs.r#type {
                    if let Integer(int) = &self.rhs.r#type {
                        if int.is_a_refinement_of(&integer::Integer::new(
                            IntegerWidth::PointerSize,
                            IntegerSign::Unsigned,
                            pointer_width,
                        ))? {
                            let ty = ty.deref().clone();
                            self.make_expression(ty, false)
                        } else {
                            Err("indexing integer may be larger than array length")
                        }
                    } else {
                        Err("expected integer for indexing rhs")
                    }
                } else {
                    Err("expected array for indexing lhs")
                }
            }
            MemberAccess => match &self.lhs.r#type {
                Struct(ident) => {
                    if let ExpressionKind::Member(member) = &self.rhs.kind {
                        let ident_type = ident_type_stack
                            .get(ident)
                            .ok_or("unable to find member")?
                            .0;

                        if let IdentType::Struct(members) = ident_type {
                            if let Some(member) = members.get(member) {
                                self.make_expression(member.to_value_type(), false)
                            } else {
                                Err("unable to find member for struct")
                            }
                        } else {
                            Err("expected identifier to be for a struct")
                        }
                    } else {
                        Err("member access was not given a member identifier")
                    }
                }
                _ => todo!(),
            },
            RightExclusiveRange => match (&self.lhs.r#type, &self.rhs.r#type) {
                (Integer(a), Integer(b)) => {
                    if a.is_same_base(b) {
                        let ty = Range(
                            a.width(),
                            a.signed(),
                            a.interval().clone(),
                            b.interval().clone(),
                            RangeKind::RightExclusive,
                        );
                        self.make_expression(ty, false)
                    } else {
                        Err("range integer types do not match")
                    }
                }
                _ => Err("expected integers for range"),
            },
            RightInclusiveRange => match (&self.lhs.r#type, &self.rhs.r#type) {
                (Integer(a), Integer(b)) => {
                    if a.is_same_base(b) {
                        let ty = Range(
                            a.width(),
                            a.signed(),
                            a.interval().clone(),
                            b.interval().clone(),
                            RangeKind::RightExclusive,
                        );
                        self.make_expression(ty, false)
                    } else {
                        Err("range integer types do not match")
                    }
                }
                _ => Err("expected integers for range"),
            },
            Add | Subtract | Multiply | Divide | Remainder => {
                match (&self.lhs.r#type, &self.rhs.r#type) {
                    (Integer(a), Integer(b)) => {
                        if a.is_same_base(b) {
                            let ty = Integer(self::integer::Integer::new_refined(
                                a.width(),
                                a.signed(),
                                pointer_width,
                                match &self.operation {
                                    Add => a.interval() + b.interval(),
                                    Subtract => a.interval() - b.interval(),
                                    Multiply => a.interval() * b.interval(),
                                    Divide => a.interval() / b.interval(),
                                    Remainder => todo!(),
                                    _ => unreachable!(),
                                },
                            )?);
                            self.make_expression(ty, false)
                        } else {
                            Err("integer types do not match")
                        }
                    }
                    _ => Err("expected integers for arithmetic operation"),
                }
            }
            Assign => {
                if self.lhs.r#type == self.rhs.r#type {
                    self.make_expression(Type::Tuple(Vec::new()), false)
                } else if let (Type::Integer(a), Type::Integer(b)) =
                    (&self.lhs.r#type, &self.rhs.r#type)
                {
                    if a.is_same_base(b) {
                        self.make_expression(Type::Tuple(Vec::new()), false)
                    } else {
                        Err("lhs of assign has a different type than the rhs")
                    }
                } else {
                    Err("lhs of assign has a different type than the rhs")
                }
            }
            _ => todo!(),
        }
    }

    fn make_expression(
        self,
        ty: Type,
        has_extra_internal_state: bool,
    ) -> Result<Expression, Error> {
        let has_internal_state = has_extra_internal_state
            || self.lhs.has_internal_state
            || self.rhs.has_internal_state;
        let has_await = self.lhs.has_await || self.rhs.has_await;

        let mut breaks = self.lhs.breaks.clone();
        for (label, ty) in &self.rhs.breaks {
            if let Some(current_ty) = breaks.get(label) {
                if current_ty != ty {
                    return Err("breaks for the same label are not the same");
                }
            } else {
                breaks.insert(label.clone(), ty.clone());
            }
        }

        let continues = self
            .lhs
            .continues
            .iter()
            .chain(&self.rhs.continues)
            .cloned()
            .collect();

        let return_type = match (&self.lhs.return_type, &self.rhs.return_type) {
            (Some(a), Some(b)) => if a != b {
                return Err("returns are not the same type")
            } else {
                Some(a.clone())
            },
            (Some(x), _) | (_, Some(x)) => Some(x.clone()),
            (None, None) => None,
        };

        let pure_inputs = self
            .lhs
            .pure_inputs
            .iter()
            .chain(self.rhs.pure_inputs.iter())
            .cloned()
            .collect();
        let in_out = self
            .lhs
            .in_out
            .iter()
            .chain(self.rhs.in_out.iter())
            .cloned()
            .collect();

        Ok(Expression {
            r#type: ty,
            kind: ExpressionKind::BinaryExpression(Box::new(self)),
            has_internal_state,
            has_await,
            pure_inputs,
            in_out,
            breaks,
            continues,
            return_type,
        }
        .normalize_inputs())
    }
}

#[test]
fn demo() {
    let ident_type_stack = IdentifierStack::new();
    let pointer_width = TargetPointerWidth::new(64);

    let a = Expression::new(
        ExpressionKind::ConstantValue(ConstantValue::Integer(
            BigInt::from(42),
            IntegerWidth::W8,
            IntegerSign::Unsigned,
        )),
        &ident_type_stack,
        pointer_width,
    )
    .unwrap();

    let b = Expression::new(
        ExpressionKind::ConstantValue(ConstantValue::Integer(
            BigInt::from(101),
            IntegerWidth::W8,
            IntegerSign::Unsigned,
        )),
        &ident_type_stack,
        pointer_width,
    )
    .unwrap();

    let c = Expression::new(
        ExpressionKind::BinaryExpression(Box::new(BinaryExpression {
            lhs: a,
            rhs: b,
            operation: BinaryOperation::Add,
        })),
        &ident_type_stack,
        pointer_width,
    )
    .unwrap();

    dbg!(c);
    // todo!()
}

#[derive(Debug)]
pub enum BinaryOperation {
    /// foo[bar]
    Index,

    /// foo(bar)
    Call,

    /// `foo.bar`
    MemberAccess,

    /// `foo + bar`
    Add,

    /// `foo - bar`
    Subtract,

    /// `for * bar`
    Multiply,

    /// `foo / bar`
    Divide,

    /// `foo % bar`
    Remainder,

    /// `foo && bar`
    And,

    /// `foo || bar`
    Or,

    /// `foo & bar`
    BitwiseAnd,

    /// `foo | bar`
    BitwiseOr,

    /// `foo ^ bar`
    BitwiseXor,

    /// `foo .. bar`
    RightExclusiveRange,

    /// `foo ..= bar`
    RightInclusiveRange,

    /// `foo << bar`
    LeftShift,

    /// `foo >> bar`
    RightShift,

    /// `foo < bar`
    LessThan,

    /// `foo <= bar`
    LessThanEqual,

    /// `foo > bar`
    GreaterThan,

    /// `foo >= bar`
    GreaterThanEqual,

    /// `foo == bar`
    Equal,

    /// `foo != bar`
    NotEqual,

    /// `|foo| bar`
    Closure,

    /// `foo as bar`
    As,

    /// `foo = bar`
    Assign,

    /// `foo::bar`
    Path,
}

/// `op expr`
#[derive(Debug)]
pub struct UnaryExpression {
    pub expression: Expression,
    pub operation: UnaryOperation,
}

#[derive(Debug)]
pub enum UnaryOperation {
    /// `-foo`
    Negative,

    /// `!foo`
    Not,

    /// `&foo`
    Borrow,

    /// `&mut foo`
    MutableBorrow,

    /// `*foo`
    Dereference,

    /// `..foo`
    RightExclusiveRange,

    /// `..=foo`
    RightInclusiveRange,

    /// `return foo`
    Return,

    /// `break foo`
    Break,
}

/// `expr op`
#[derive(Debug)]
pub struct FollowingUnaryExpression {
    pub expression: Expression,
    pub operation: FollowingUnaryOperation,
}

#[derive(Debug)]
pub enum FollowingUnaryOperation {
    /// `foo..`
    RightExclusiveRange,

    /// `foo?`
    Propagate,

    /// `foo.await`
    Await,

    /// foo()
    Call,
}

/// `op`
#[derive(Debug)]
pub enum Operation {
    /// `..`
    RightExclusiveRange,

    /// `return`
    Return,

    /// `continue`
    Continue,

    /// `break`
    Break,
}

#[derive(Debug)]
pub struct Field {}
#[derive(Debug)]
pub struct Function {}
#[derive(Debug)]
pub struct If {}
#[derive(Debug)]
pub struct Iterate {}
#[derive(Debug)]
pub struct Jump {}
#[derive(Debug)]
pub struct Return {}
#[derive(Debug)]
pub struct Status {}
#[derive(Debug)]
pub struct Struct {}
#[derive(Debug)]
pub struct TypeExpression {}
#[derive(Debug)]
pub struct Variable {}
#[derive(Debug)]
pub struct While {}
