use std::collections::BTreeMap;

use crate::interval::IntegerInterval;

use super::{BasicFact, Binding, BindingOrValue, Comparison, Fact, Proof, Type};

/// A fact proven my application of an axiom.
///
/// Example:
/// ```
/// prove! {
///     index == length
///     via (a == b given a == c and c == b)
///     with c := other
/// }
/// ```
#[derive(Clone, Debug)]
pub struct FactViaAxiom {
    /// The fact that the axiom proves.
    fact: BasicFact,

    /// The axiom to use to prove the above fact.
    axiom: Axiom,

    /// Mappings from the axiom generic variables to actual bindings.
    mappings: BTreeMap<AxiomGeneric, BindingOrValue>,
}

impl FactViaAxiom {
    pub fn mentions(&self, binding: &Binding) -> bool {
        if self.fact.mentions(binding) {
            return true;
        }

        for (_, binding_or_value) in &self.mappings {
            match binding_or_value {
                BindingOrValue::Binding(x) => {
                    if x == binding {
                        return true;
                    }
                }
                BindingOrValue::Integer(_) => {}
            }
        }

        false
    }

    pub fn proves_follows<'a, 'b>(&'a self, facts: &'b [Fact]) -> Option<Proof<'a>> {
        // Need to prove each of the given.
        let mut given_facts = Vec::new();
        for given in &self.axiom.given {
            match given {
                AxiomFact::Cmp(a, b, cmp) => {
                    // Get interval of a.
                    let a_interval = match a {
                        AxiomFactExpression::Variable(generic) => {
                            match &self.mappings[generic] {
                                BindingOrValue::Binding(binding) => {
                                    // Prove binding is a integer.
                                    let a_is_integer =
                                        prove_binding_is_integer(binding, facts)?;
                                    given_facts.push(a_is_integer);

                                    // Find fact 
                                }
                                BindingOrValue::Integer(_) => todo!(),
                            }
                        }
                        AxiomFactExpression::Add(_, _) => todo!(),
                        AxiomFactExpression::Sub(_, _) => todo!(),
                    };
                }
            }
        }
        Some(Proof {
            evidence: given_facts,
        })
    }
}

fn prove_binding_is_integer<'a, 'b>(
    binding: &'a Binding,
    facts: &'b [Fact],
) -> Option<&'b Fact> {
    for fact in facts {
        match fact {
            Fact::Basic(basic) => match basic {
                BasicFact::Exists(known, ty) => {
                    if known == binding && ty == &Type::Integer {
                        return Some(fact);
                    }
                }
                _ => {}
            },
            Fact::ViaAxiom(_) => {}
        }
    }
    None
}

fn find_binding_interval<'a, 'b>(
    binding: &'a Binding,
    facts: &'b [Fact],
) -> Option<(IntegerInterval, Vec<&'b Fact>)> {
    for fact in facts.iter().rev() {
        match fact {
            Fact::Basic(basic) => match basic {
                BasicFact::Exists(known, ty) => {
                    if known == binding && ty == &Type::Integer {
                        return Some((IntegerInterval::Open, vec![fact]));
                    }
                }
                BasicFact::InIntegerInterval(known, interval) => {
                    if known == binding {
                        return Some((interval.clone(), vec![fact]));
                    }
                }
                BasicFact::Cmp(a, b, cmp) => {
                    if a == &BindingOrValue::Binding(binding.clone()) {
                        let (b_interval, mut proof_facts) = match b {
                            BindingOrValue::Binding(b) => {
                                find_binding_interval(b, facts)?
                            }
                            BindingOrValue::Integer(value) => {
                                (IntegerInterval::Single(value.clone()), Vec::new())
                            }
                        };
                        proof_facts.push(fact);
                        let a_interval = match cmp {
                            Comparison::Less => match b_interval {
                                IntegerInterval::Empty => IntegerInterval::Empty,
                                IntegerInterval::Single(value) => {
                                    IntegerInterval::OpenLeft { end: value - 1 }
                                }
                                IntegerInterval::Bound { start, end } => {
                                    IntegerInterval::OpenLeft { end: end - 1 }
                                }
                                IntegerInterval::OpenRight { start } => {
                                    IntegerInterval::Open
                                }
                                IntegerInterval::OpenLeft { end } => {
                                    IntegerInterval::OpenLeft { end: end - 1 }
                                }
                                IntegerInterval::Open => IntegerInterval::Empty,
                            },
                            Comparison::LessEqual => match b_interval {
                                IntegerInterval::Empty => IntegerInterval::Empty,
                                IntegerInterval::Single(value) => {
                                    IntegerInterval::OpenLeft { end: value }
                                }
                                IntegerInterval::Bound { start, end } => {
                                    IntegerInterval::OpenLeft { end: start }
                                }
                                IntegerInterval::OpenRight { start } => {
                                    IntegerInterval::OpenLeft { end: start - 1 }
                                }
                                IntegerInterval::OpenLeft { end } => {
                                    IntegerInterval::Empty
                                }
                                IntegerInterval::Open => IntegerInterval::Empty,
                            },
                            Comparison::Equal => return Some((b_interval, proof_facts)),
                            Comparison::NotEqual => todo!(),
                            Comparison::GreaterEqual => todo!(),
                            Comparison::Greater => todo!(),
                        };
                        return Some((a_interval, proof_facts));
                    }
                }
                _ => {}
            },
            Fact::ViaAxiom(_) => {}
        }
    }
    None
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
#[non_exhaustive]
pub enum AxiomGeneric {
    A,
    B,
    C,
}

impl std::fmt::Display for AxiomGeneric {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            AxiomGeneric::A => write!(f, "A"),
            AxiomGeneric::B => write!(f, "B"),
            AxiomGeneric::C => write!(f, "C"),
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Axiom {
    /// The fact proven by the axiom.
    proves: AxiomFact,

    /// The required facts for the above fact to be proven by the axiom.
    given: Vec<AxiomFact>,
}

impl Axiom {
    pub fn new(proves: AxiomFact, given: Vec<AxiomFact>) -> Self {
        Self { proves, given }
    }
}

impl std::fmt::Display for Axiom {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut iter = self.given.iter();
        write!(
            f,
            "{} given {}",
            self.proves,
            iter.next().ok_or(std::fmt::Error)?
        )?;

        for fact in iter {
            write!(f, " and {}", fact)?;
        }

        Ok(())
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum AxiomFact {
    Cmp(AxiomFactExpression, AxiomFactExpression, Comparison),
}

impl std::fmt::Display for AxiomFact {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            AxiomFact::Cmp(a, b, Comparison::Less) => write!(f, "{a} < {b}"),
            AxiomFact::Cmp(a, b, Comparison::LessEqual) => write!(f, "{a} <= {b}"),
            AxiomFact::Cmp(a, b, Comparison::Equal) => write!(f, "{a} == {b}"),
            AxiomFact::Cmp(a, b, Comparison::NotEqual) => write!(f, "{a} != {b}"),
            AxiomFact::Cmp(a, b, Comparison::GreaterEqual) => write!(f, "{a} >= {b}"),
            AxiomFact::Cmp(a, b, Comparison::Greater) => write!(f, "{a} > {b}"),
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum AxiomFactExpression {
    Variable(AxiomGeneric),
    Add(AxiomGeneric, AxiomGeneric),
    Sub(AxiomGeneric, AxiomGeneric),
}

impl std::fmt::Display for AxiomFactExpression {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            AxiomFactExpression::Add(a, b) => write!(f, "({a} + {b})"),
            AxiomFactExpression::Sub(a, b) => write!(f, "({a} - {b})"),
            AxiomFactExpression::Variable(a) => write!(f, "{a}"),
        }
    }
}

#[macro_export]
macro_rules! axiom_fact_literal {
    ($a:tt < $b:tt) => {{
        AxiomFact::Cmp(
            axiom_fact_literal!($a),
            axiom_fact_literal!($b),
            Comparison::Less,
        )
    }};
    ($a:tt > $b:tt) => {{
        AxiomFact::Cmp(
            axiom_fact_literal!($a),
            axiom_fact_literal!($b),
            Comparison::Greater,
        )
    }};
    ($a:tt == $b:tt) => {{
        AxiomFact::Cmp(
            axiom_fact_literal!($a),
            axiom_fact_literal!($b),
            Comparison::Equal,
        )
    }};
    ($a:tt <= $b:tt) => {{
        AxiomFact::Cmp(
            axiom_fact_literal!($a),
            axiom_fact_literal!($b),
            Comparison::LessEqual,
        )
    }};
    ($a:tt >= $b:tt) => {{
        AxiomFact::Cmp(
            axiom_fact_literal!($a),
            axiom_fact_literal!($b),
            Comparison::GreaterEqual,
        )
    }};
    ($a:tt != $b:tt) => {{
        AxiomFact::Cmp(
            axiom_fact_literal!($a),
            axiom_fact_literal!($b),
            Comparison::NotEqual,
        )
    }};
    (($a:tt + $b:tt)) => {{
        AxiomFactExpression::Add(
            axiom_fact_literal!(generic $a),
            axiom_fact_literal!(generic $b)
        )
    }};
    (($a:tt - $b:tt)) => {{
        AxiomFactExpression::Sub(
            axiom_fact_literal!(generic $a),
            axiom_fact_literal!(generic $b)
        )
    }};
    ($x:tt) => {{
        AxiomFactExpression::Variable(axiom_fact_literal!(generic $x))
    }};
    (generic $x:tt) => {{
        AxiomGeneric::$x
    }};
}

#[macro_export]
macro_rules! axiom_literal {
    ({$($fact:tt)*} : {$($given1:tt)*} $(; {$($given_x:tt)*})*) => {{
        Axiom::new(
            axiom_fact_literal!($($fact)*),
            vec![
                axiom_fact_literal!($($given1)*),
                $(axiom_fact_literal!($($given_x)*)),*
            ]
        )
    }};
}

#[test]
fn demo() {
    let a = Axiom {
        proves: AxiomFact::Cmp(
            AxiomFactExpression::Add(AxiomGeneric::A, AxiomGeneric::B),
            AxiomFactExpression::Variable(AxiomGeneric::C),
            Comparison::LessEqual,
        ),
        given: vec![
            AxiomFact::Cmp(
                AxiomFactExpression::Variable(AxiomGeneric::C),
                AxiomFactExpression::Sub(AxiomGeneric::A, AxiomGeneric::B),
                Comparison::Greater,
            ),
            AxiomFact::Cmp(
                AxiomFactExpression::Variable(AxiomGeneric::C),
                AxiomFactExpression::Variable(AxiomGeneric::A),
                Comparison::NotEqual,
            ),
        ],
    };

    let b = axiom_literal!({(A + B) <= C}: {C > (A - B)}; {C != A});

    assert_eq!(a, b);
}
