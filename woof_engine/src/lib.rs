// mod fact;
mod ast;
mod parser;

use std::collections::VecDeque;

use interval::IntegerInterval;
use proc_macro2::{Span, TokenStream};

pub mod interval;

// use fact::Fact;

// pub struct Module {
//     original_source: TokenStream,
// }
//
// impl Module {
//     pub fn from_source(source: TokenStream) -> Result<Self, LocalizedError> {
//         todo!()
//     }
// }

// pub enum Error {
//     Syntax(SyntaxError),
//     BrokenInvariant(BrokenInvariantError),
// }

pub struct SyntaxError {
    message: String,
}

// pub struct BrokenInvariantError {
//     known_facts: Vec<Fact>,
//     invalid_fact: Fact,
//     message: String,
// }

// pub struct LocalizedError {
//     error: Error,
//     cause_span: Span,
//     cause_tokens: TokenStream,
// }

/*
#[derive(Clone)]
pub struct FunctionContract {
    /// Prove statements before any other statement that define the
    /// function's contract.
    pub facts: Vec<Fact>,

    /// Marker for if the function is allowed to access global state or
    /// other non-pure functions.
    pub is_pure: bool,

    /// Marker for if the function is allowed to suspend and resume.
    pub is_coroutine: bool,

    /// Marker for if the function is defined outside of the active woof analysis.
    pub is_external: bool,

    /// Marker for if the function will be public outside the module.
    pub is_public: bool,
}

type Path = Vec<String>;

/// All known contracts.
#[derive(Clone)]
pub struct BaseContext {
    function_contracts: Vec<(Vec<Path>, FunctionContract)>,
}

/// A proof context for a specific function.
#[derive(Clone)]
pub struct FunctionContext<'a> {
    /// Name for debugging and errors.
    name: String,

    /// Base context this analysis can pull from.
    base: &'a BaseContext,

    /// Base contract of the function under analysis.
    contract: &'a FunctionContract,

    /// Fact lists to return to when branch is done.
    branch_stack: VecDeque<Vec<Fact>>,

    /// Facts currently known.
    active_facts: Vec<Fact>,
}

impl Fact {
    pub fn references_return(&self) -> bool {
        match self {
            Fact::Exists(Binding::Return, _) => true,
            Fact::InIntegerInterval(Binding::Return, _) => true,
            _ => false,
        }
    }
}

impl FunctionContract {
    pub fn pre_facts(&self) -> impl Iterator<Item = &'_ Fact> + '_ {
        self.facts
            .iter()
            .filter(|fact| !Fact::references_return(fact))
    }

    pub fn post_facts(&self) -> impl Iterator<Item = &'_ Fact> + '_ {
        self.facts
            .iter()
            .filter(|fact| Fact::references_return(fact))
    }
}

impl BaseContext {
    pub fn new() -> Self {
        Self {
            function_contracts: Vec::new(),
        }
    }

    pub fn add_function_contract<T>(&mut self, paths: T, contract: FunctionContract)
    where
        T: IntoIterator<Item = Vec<String>>,
    {
        let paths = paths.into_iter().collect();
        self.function_contracts.push((paths, contract))
    }
}

#[derive(Clone, Debug)]
pub struct Explanation(pub String);

#[derive(Clone, Debug)]
pub enum Statement {
    /// A stand-alone expression.
    Expr(Expression),
}

#[derive(Clone, Debug)]
pub enum Expression {
    /// Addition of two expressions: `a + b`
    Add(Box<Expression>, Box<Expression>),

    /// A usage of a variable: `a`
    Var(String),
}

impl<'a> FunctionContext<'a> {
    pub fn new(
        base: &'a BaseContext,
        name: String,
        contract: &'a FunctionContract,
    ) -> Self {
        let active_facts = contract.pre_facts().cloned().collect();
        Self {
            name,
            base,
            contract,
            branch_stack: VecDeque::new(),
            active_facts,
        }
    }

    pub fn active_facts(&self) -> impl Iterator<Item = &Fact> {
        self.active_facts.iter()
    }

    /// This is the final statement of the function.
    ///
    /// This will prove the statment is valid and that the return
    /// contract is satisfied.
    pub fn finish(mut self, statement: Statement) -> Result<(), Explanation> {
        if self
            .contract
            .post_facts()
            .find(|fact| match fact {
                Fact::Exists(Binding::Return, _) => true,
                _ => false,
            })
            .is_none()
        {
            return Err(Explanation(
                "No exist fact found for return. A function must always return a value."
                    .into(),
            ));
        }

        match statement {
            Statement::Expr(expr) => {
                let value = self.prove_expression(expr)?;

                dbg!(&value);

                // verify value satisifies the return facts.
                for fact in self.contract.post_facts() {
                    match fact {
                        Fact::Exists(_, ty) => match ty {
                            Type::Integer(known_interval) => match &value {
                                ExpressionValue::Integer { interval } => {
                                    if !interval.is_subinterval_of(known_interval) {
                                        return Err(Explanation(format!("Returned integer has a possible range of {}, but the return type only has a possible range of {}.", interval, known_interval)));
                                    }
                                }
                            },
                            Type::Custom(_) => todo!(),
                        },
                        Fact::InIntegerInterval(_, known_interval) => match &value {
                            ExpressionValue::Integer { interval } => {
                                if !interval.is_subinterval_of(known_interval) {
                                    return Err(Explanation(format!("Returned integer has a possible range of {}, but the return type only has a possible range of {}.", interval, known_interval)));
                                }
                            }
                        },
                    }
                }

                Ok(())
            }
        }
    }

    fn prove_expression(
        &mut self,
        expression: Expression,
    ) -> Result<ExpressionValue, Explanation> {
        let mut value_stack = VecDeque::new();

        let mut expression_stack = VecDeque::new();
        expression_stack.push_back(ExpressionStackItem::Do(expression));

        while let Some(expression) = expression_stack.pop_back() {
            match expression {
                ExpressionStackItem::Do(expression) => match expression {
                    Expression::Add(a, b) => {
                        // do a then b, then adjust facts
                        expression_stack.push_back(ExpressionStackItem::Finish(
                            ExpressionFinish::Add,
                        ));
                        expression_stack.push_back(ExpressionStackItem::Do(*b));
                        expression_stack.push_back(ExpressionStackItem::Do(*a));
                    }
                    Expression::Var(name) => {
                        // lookup variable from context facts.
                        if let Some(ty) = self
                            .active_facts()
                            .flat_map(|fact| match fact {
                                Fact::Exists(Binding::Variable(var_name), ty)
                                    if var_name == &name =>
                                {
                                    Some(ty)
                                }
                                _ => None,
                            })
                            .next()
                        {
                            match ty {
                                Type::Integer(interval) => {
                                    // lookup interval from known facts.
                                    let refined_interval = self
                                        .active_facts()
                                        .flat_map(|fact| match fact {
                                            Fact::InIntegerInterval(
                                                Binding::Variable(var_name),
                                                interval,
                                            ) if var_name == &name => Some(interval),
                                            _ => None,
                                        })
                                        .next()
                                        .unwrap_or(interval);

                                    value_stack.push_back(ExpressionValue::Integer {
                                        interval: refined_interval.clone(),
                                    });
                                }
                                Type::Custom(_) => todo!(),
                            }
                        } else {
                            return Err(Explanation(format!(
                                "Variable {} doesn't exist",
                                name
                            )));
                        }
                    }
                },
                ExpressionStackItem::Finish(expression) => match expression {
                    ExpressionFinish::Add => {
                        let b = value_stack.pop_back().ok_or_else(|| {
                            Explanation("Expected value for rhs of addition".into())
                        })?;
                        let a = value_stack.pop_back().ok_or_else(|| {
                            Explanation("Expected value for lhs of addition.".into())
                        })?;

                        match (a, b) {
                            (ExpressionValue::Integer { interval: a }, ExpressionValue::Integer { interval: b }) => {
                                value_stack.push_back(ExpressionValue::Integer { interval: &a + &b })
                            },
                        }
                    }
                },
            }
        }

        if value_stack.len() > 1 {
            return Err(Explanation(format!(
                "Expression resulted in multiple values."
            )));
        }

        value_stack
            .pop_back()
            .ok_or_else(|| Explanation(format!("Expression didn't result in a value.")))
    }
}

#[derive(Clone, Debug)]
enum ExpressionStackItem {
    Do(Expression),
    Finish(ExpressionFinish),
}

#[derive(Clone, Debug)]
enum ExpressionFinish {
    Add,
}

#[derive(Clone, Debug)]
enum ExpressionValue {
    Integer { interval: IntegerInterval },
}
*/
