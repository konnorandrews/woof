mod axiom;

use num_bigint::BigInt;
use quote::quote;
use std::collections::VecDeque;

use proc_macro2::TokenStream;

use crate::interval::IntegerInterval;

use self::axiom::FactViaAxiom;

#[derive(Clone, Debug, PartialEq)]
pub enum Type {
    /// Same of a custom struct or enum.
    Custom(String),

    Reference(Mutability),
    Pointer(Mutability),
    Integer,
    Unit,
    Bool,
    IoReader,
    IoWriter,
}

#[derive(Clone, Debug, PartialEq)]
pub enum Mutability {
    Immutable,
    Mutable,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Comparison {
    Less,
    LessEqual,
    Equal,
    NotEqual,
    GreaterEqual,
    Greater,
}

/// A boolean statement guaranteed to be true at a given point.
#[derive(Clone, Debug)]
pub enum BasicFact {
    /// The given binding exists with a given type.
    Exists(Binding, Type),

    /// The integer typed binding is in the given interval.
    InIntegerInterval(Binding, IntegerInterval),

    BoolIsValue(Binding, bool),

    PointerIsNull(Binding, bool),

    Cmp(BindingOrValue, BindingOrValue, Comparison),
}

impl BasicFact {
    pub fn mentions(&self, binding: &Binding) -> bool {
        match self {
            BasicFact::Exists(x, _) => x == binding,
            BasicFact::InIntegerInterval(x, _) => x == binding,
            BasicFact::BoolIsValue(x, _) => x == binding,
            BasicFact::PointerIsNull(x, _) => x == binding,
            BasicFact::Cmp(a, b, _) => {
                match a {
                    BindingOrValue::Binding(x) => {
                        if x == binding {
                            return true;
                        }
                    }
                    BindingOrValue::Integer(_) => {}
                }

                match b {
                    BindingOrValue::Binding(x) => {
                        if x == binding {
                            return true;
                        }
                    }
                    BindingOrValue::Integer(_) => {}
                }

                false
            }
        }
    }
}

#[derive(Clone, Debug)]
pub enum Fact {
    Basic(BasicFact),
    ViaAxiom(FactViaAxiom),
}

impl Fact {
    pub fn mentions(&self, binding: &Binding) -> bool {
        match self {
            Fact::Basic(basic) => basic.mentions(binding),
            Fact::ViaAxiom(via_axiom) => via_axiom.mentions(binding),
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum Binding {
    /// A generic variable with a given name.
    Variable(String),

    /// The special return value from a function.
    Return,
}

#[derive(Clone, Debug, PartialEq)]
pub enum BindingOrValue {
    Binding(Binding),
    Integer(BigInt),
}

#[derive(Clone, Debug)]
pub struct Situation {
    facts: Vec<Fact>,
}

impl Situation {
    pub fn new() -> Self {
        Self { facts: Vec::new() }
    }

    fn normalize(&mut self) {
        let mut i = 0;
        while i < self.facts.len() {
            let can_prove = match &self.facts[i] {
                Fact::Basic(_) => true,
                Fact::ViaAxiom(via_axiom) => {
                    via_axiom.prove_follows(&self.facts[..i]).is_some()
                },
            };

            if !can_prove {
                self.facts.remove(i);
                i -= 1;
            }
        }
    }

    pub fn make_known(&mut self, fact: Fact) {
        match &fact {
            Fact::Basic(basic) => match basic {
                BasicFact::Exists(binding, ty) => {
                    // A exists removes any fact that mentions the binding if the type changed.
                    if let Some(existing_type) = self
                        .facts
                        .iter()
                        .flat_map(|known| match known {
                            Fact::Basic(basic) => match basic {
                                BasicFact::Exists(known_binding, ty) => {
                                    if known_binding == binding {
                                        Some(ty)
                                    } else {
                                        None
                                    }
                                }
                                _ => None,
                            },
                            Fact::ViaAxiom(_) => None,
                        })
                        .next()
                    {
                        if existing_type == ty {
                            // do nothing
                        } else {
                            // remove all mentions
                            let mut i = 0;
                            while i < self.facts.len() {
                                if self.facts[i].mentions(binding) {
                                    self.facts.remove(i);
                                    i -= 1;
                                }
                            }
                            
                            // normalize
                            self.normalize();
                        }
                    } else {
                    }
                }
                BasicFact::InIntegerInterval(_, _) => todo!(),
                BasicFact::BoolIsValue(_, _) => todo!(),
                BasicFact::PointerIsNull(_, _) => todo!(),
                BasicFact::Cmp(_, _, _) => todo!(),
            },
            Fact::ViaAxiom(via_axiom) => todo!(),
        }
    }

    pub fn prove_basic_fact_follows<'a, 'b>(
        &'a self,
        fact: &'b BasicFact,
    ) -> Option<Proof<'a>> {
        match fact {
            BasicFact::Exists(binding, ty) => {
                // An exists is proven by having an exact match in the known facts.
                if let Some(known) = self.facts.iter().find(|known| {
                    if let Fact::Basic(BasicFact::Exists(known_binding, known_ty)) = known
                    {
                        known_binding == binding && known_ty == ty
                    } else {
                        false
                    }
                }) {
                    Some(Proof {
                        evidence: vec![known],
                    })
                } else {
                    None
                }
            }
            BasicFact::InIntegerInterval(_, _) => todo!(),
            BasicFact::BoolIsValue(_, _) => todo!(),
            BasicFact::PointerIsNull(_, _) => todo!(),
            BasicFact::Cmp(_, _, _) => todo!(),
        }
    }
}

#[derive(Clone, Debug)]
pub struct Proof<'a> {
    evidence: Vec<&'a Fact>,
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn prove_exists() {
        let mut sit = Situation::new();
        sit.make_known(Fact::Basic(BasicFact::Exists(
            Binding::Variable("test".to_owned()),
            Type::Integer,
        )));
    }
}
