use num_bigint::BigInt;
use proc_macro2::Span;
use quote::quote;
use std::collections::HashMap;

use syn::{parse_quote, Block, Expr, Stmt};

use crate::{
    ast::{
        self,
        ident_stack::IdentifierStack,
        integer::{Integer, IntegerSign, IntegerWidth, TargetPointerWidth},
        BinaryExpression, BinaryOperation, ConstantValue, Expression, ExpressionKind,
        IdentType, Identifier, Node, Type, Operation, UnaryExpression, UnaryOperation,
    },
    interval::IntegerInterval,
};

#[derive(Debug)]
pub struct Context {
    identifier_map: HashMap<syn::Ident, Identifier>,
    ident_type_stack: IdentifierStack<'static, IdentType>,
    pointer_width: TargetPointerWidth,
}

impl Context {
    pub fn parse_expression(&mut self, expr: &Expr) -> Result<Expression, crate::ast::Error> {
        use Expr::*;
        let kind = match expr {
            Array(expr) => ExpressionKind::ConstantValue(ConstantValue::Array(
                expr.elems
                    .iter()
                    .map(|expr| self.parse_expression(expr))
                    .collect::<Result<_, _>>()?,
            )),
            Assign(expr) => {
                ExpressionKind::BinaryExpression(std::boxed::Box::new(BinaryExpression {
                    lhs: self.parse_expression(&expr.left)?,
                    rhs: self.parse_expression(&expr.right)?,
                    operation: BinaryOperation::Assign,
                }))
            }
            AssignOp(_) => return Err("compound assignment expressions are not allowed"),
            Async(_) => todo!(),
            Await(_) => todo!(),
            Binary(expr) => {
                ExpressionKind::BinaryExpression(std::boxed::Box::new(BinaryExpression {
                    lhs: self.parse_expression(&expr.left)?,
                    rhs: self.parse_expression(&expr.right)?,
                    operation: match expr.op {
                        syn::BinOp::Add(_) => BinaryOperation::Add,
                        syn::BinOp::Sub(_) => BinaryOperation::Subtract,
                        syn::BinOp::Mul(_) => BinaryOperation::Multiply,
                        syn::BinOp::Div(_) => BinaryOperation::Divide,
                        syn::BinOp::Rem(_) => BinaryOperation::Remainder,
                        syn::BinOp::And(_) => BinaryOperation::And,
                        syn::BinOp::Or(_) => BinaryOperation::Or,
                        syn::BinOp::BitXor(_) => BinaryOperation::BitwiseXor,
                        syn::BinOp::BitAnd(_) => BinaryOperation::BitwiseAnd,
                        syn::BinOp::BitOr(_) => BinaryOperation::BitwiseOr,
                        syn::BinOp::Shl(_) => BinaryOperation::LeftShift,
                        syn::BinOp::Shr(_) => BinaryOperation::RightShift,
                        syn::BinOp::Eq(_) => BinaryOperation::Equal,
                        syn::BinOp::Lt(_) => BinaryOperation::LessThan,
                        syn::BinOp::Le(_) => BinaryOperation::LessThanEqual,
                        syn::BinOp::Ne(_) => BinaryOperation::NotEqual,
                        syn::BinOp::Ge(_) => BinaryOperation::GreaterThanEqual,
                        syn::BinOp::Gt(_) => BinaryOperation::GreaterThan,
                        syn::BinOp::AddEq(_) => return Err("+= operator is not allowed"),
                        syn::BinOp::SubEq(_) => return Err("-= operator is not allowed"),
                        syn::BinOp::MulEq(_) => return Err("*= operator is not allowed"),
                        syn::BinOp::DivEq(_) => return Err("/= operator is not allowed"),
                        syn::BinOp::RemEq(_) => return Err("%= operator is not allowed"),
                        syn::BinOp::BitXorEq(_) => {
                            return Err("^= operator is not allowed")
                        }
                        syn::BinOp::BitAndEq(_) => {
                            return Err("&= operator is not allowed")
                        }
                        syn::BinOp::BitOrEq(_) => {
                            return Err("|= operator is not allowed")
                        }
                        syn::BinOp::ShlEq(_) => {
                            return Err("<<= operator is not allowed")
                        }
                        syn::BinOp::ShrEq(_) => {
                            return Err(">>= operator is not allowed")
                        }
                    },
                }))
            }
            Block(expr) => ExpressionKind::Block(self.parse_block(&expr.block)?),
            Box(_) => return Err("box expressions are not allowed"),
            Break(expr) => {
                if let Some(expr) = &expr.expr {
                    ExpressionKind::UnaryExpression(std::boxed::Box::new(UnaryExpression {
                        expression: self.parse_expression(&*expr)?,
                        operation: UnaryOperation::Break,
                    }))
                } else {
                    ExpressionKind::Operation(Operation::Break)
                }
            },
            Call(_) => todo!(),
            Cast(_) => todo!(),
            Closure(_) => todo!(),
            Continue(_) => todo!(),
            Field(_) => todo!(),
            ForLoop(_) => todo!(),
            Group(_) => todo!(),
            If(_) => todo!(),
            Index(_) => todo!(),
            Let(_) => todo!(),
            Lit(expr) => match &expr.lit {
                syn::Lit::Str(_) => todo!(),
                syn::Lit::ByteStr(_) => todo!(),
                syn::Lit::Byte(_) => todo!(),
                syn::Lit::Char(_) => todo!(),
                syn::Lit::Int(lit) => {
                    let value = lit
                        .base10_parse()
                        .map_err(|_| "unable to parse int literal")?;
                    ExpressionKind::ConstantValue(match lit.suffix() {
                        "i8" => ConstantValue::Integer(
                            value,
                            IntegerWidth::W8,
                            IntegerSign::Signed,
                        ),
                        "i16" => ConstantValue::Integer(
                            value,
                            IntegerWidth::W16,
                            IntegerSign::Signed,
                        ),
                        "i32" => ConstantValue::Integer(
                            value,
                            IntegerWidth::W32,
                            IntegerSign::Signed,
                        ),
                        "i64" => ConstantValue::Integer(
                            value,
                            IntegerWidth::W64,
                            IntegerSign::Signed,
                        ),
                        "i128" => ConstantValue::Integer(
                            value,
                            IntegerWidth::W128,
                            IntegerSign::Signed,
                        ),
                        "u8" => ConstantValue::Integer(
                            value,
                            IntegerWidth::W8,
                            IntegerSign::Unsigned,
                        ),
                        "u16" => ConstantValue::Integer(
                            value,
                            IntegerWidth::W16,
                            IntegerSign::Unsigned,
                        ),
                        "u32" => ConstantValue::Integer(
                            value,
                            IntegerWidth::W32,
                            IntegerSign::Unsigned,
                        ),
                        "u64" => ConstantValue::Integer(
                            value,
                            IntegerWidth::W64,
                            IntegerSign::Unsigned,
                        ),
                        "u128" => ConstantValue::Integer(
                            value,
                            IntegerWidth::W128,
                            IntegerSign::Unsigned,
                        ),
                        "isize" => ConstantValue::Integer(
                            value,
                            IntegerWidth::PointerSize,
                            IntegerSign::Signed,
                        ),
                        "usize" => ConstantValue::Integer(
                            value,
                            IntegerWidth::PointerSize,
                            IntegerSign::Unsigned,
                        ),
                        _ => return Err("missing or invalid integer suffix"),
                    })
                }
                syn::Lit::Float(_) => todo!(),
                syn::Lit::Bool(_) => todo!(),
                syn::Lit::Verbatim(_) => todo!(),
            },
            Loop(_) => todo!(),
            Macro(_) => todo!(),
            Match(_) => todo!(),
            MethodCall(_) => todo!(),
            Paren(_) => todo!(),
            Path(expr) => match expr.path.segments.len() {
                0 => unreachable!(),
                1 => ExpressionKind::Identifier(
                    self.identifier_map
                        .get(&expr.path.segments.first().unwrap().ident)
                        .ok_or("unknown identifier")?
                        .clone(),
                ),
                _ => {
                    let mut current = Some(ExpressionKind::GlobalIdentifier(
                        self.identifier_map
                            .get(&expr.path.segments.first().unwrap().ident)
                            .ok_or("unknown identifier")?
                            .clone(),
                    ));

                    for segment in expr.path.segments.iter().skip(1) {
                        current = Some(ExpressionKind::BinaryExpression(
                            std::boxed::Box::new(BinaryExpression {
                                lhs: Expression::new(
                                    current.take().unwrap(),
                                    &self.ident_type_stack,
                                    self.pointer_width,
                                )?,
                                rhs: Expression::new(
                                    ExpressionKind::GlobalIdentifier(
                                        self.identifier_map
                                            .get(&segment.ident)
                                            .ok_or("unknown identifier")?
                                            .clone(),
                                    ),
                                    &self.ident_type_stack,
                                    self.pointer_width,
                                )?,
                                operation: BinaryOperation::Path,
                            }),
                        ))
                    }
                    current.unwrap()
                }
            },
            Range(_) => todo!(),
            Reference(_) => todo!(),
            Repeat(_) => todo!(),
            Return(_) => todo!(),
            Struct(_) => todo!(),
            Try(_) => todo!(),
            TryBlock(_) => todo!(),
            Tuple(_) => todo!(),
            Type(_) => todo!(),
            Unary(_) => todo!(),
            Unsafe(_) => todo!(),
            Verbatim(_) => todo!(),
            While(_) => todo!(),
            Yield(_) => todo!(),
            _ => todo!(),
        };

        Expression::new(kind, &self.ident_type_stack, self.pointer_width)
    }

    pub fn parse_block(
        &mut self,
        block: &Block,
    ) -> Result<crate::ast::Block, crate::ast::Error> {
        let mut nodes = Vec::new();

        self.ident_type_stack.start_scope();

        for stmt in &block.stmts {
            match stmt {
                Stmt::Local(_) => todo!(),
                Stmt::Item(_) => todo!(),
                Stmt::Expr(expr) => {
                    nodes.push(Node::Expression(self.parse_expression(expr)?))
                }
                Stmt::Semi(expr, _) => {
                    nodes.push(Node::Expression(self.parse_expression(expr)?))
                },
            }
        }

        self.ident_type_stack.end_scope();

        Ok(ast::Block::new(nodes))
    }
}

#[test]
fn can_parse_expressions() {
    let mut context = Context {
        identifier_map: HashMap::new(),
        ident_type_stack: IdentifierStack::new(),
        pointer_width: TargetPointerWidth::new(64),
    };

    context.identifier_map.insert(
        syn::Ident::new("foo", Span::call_site()),
        Identifier::new(2),
    );

    context.ident_type_stack.start_scope();
    context
        .ident_type_stack
        .add_identifier(
            Identifier::new(2),
            IdentType::ValueType(Type::Integer(Integer::new(
                IntegerWidth::W8,
                IntegerSign::Unsigned,
                context.pointer_width,
            ))),
        )
        .unwrap();

    context
        .parse_expression(&parse_quote!([1i32, 2i32, 3i32]))
        .unwrap();
    context.parse_expression(&parse_quote!(foo = 42u8)).unwrap();
    context
        .parse_expression(&parse_quote!(101i32 + 42i32))
        .unwrap();
    context
        .parse_expression(&parse_quote!({foo = 101u8; 1u8}))
        .unwrap();
    let x = context
        .parse_expression(&parse_quote!({foo = { break 10i32; }; 1u8}))
        .unwrap();
    dbg!(x);
    todo!()
}
