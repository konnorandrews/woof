use std::collections::HashMap;

use super::Identifier;

#[derive(Debug)]
pub struct IdentifierScope<T> {
    values: HashMap<Identifier, T>,
}

impl<T> IdentifierScope<T> {
    pub fn get<'a>(&'a self, ident: &'_ Identifier) -> Option<&'a T> {
        self.values.get(ident)
    }

    pub fn idents(&self) -> impl Iterator<Item = &Identifier> {
        self.values.keys()
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub struct IdentifierScopeId {
    /// Number of scopes in.
    depth: usize,

    /// Number of sibling scopes down.
    count: usize,
}

impl IdentifierScopeId {
    /// Check if this scope ID is for a scope inside of another scope ID's scope.
    ///
    /// The returned value only has meaning if the scope IDs are from the same
    /// `IdentifierTypeStack` or a child stack.
    pub fn is_inside_of(&self, other: IdentifierScopeId) -> bool {
        self.depth > other.depth
    }

    /// Check if the scope ID is for the empty super scope.
    pub fn is_null(&self) -> bool {
        self.depth == 0
    }
}

#[derive(Debug)]
pub struct IdentifierStack<'a, T> {
    parent: Option<&'a IdentifierStack<'a, T>>,

    current_scope_depth: usize,
    current_sibling_count: usize,

    /// Stack where each item is the identifiers defined in that block.
    scopes: Vec<(IdentifierScope<T>, usize)>,
}

impl<T> IdentifierStack<'static, T> {
    pub fn new() -> Self {
        Self {
            scopes: Vec::new(),
            current_scope_depth: 0,
            current_sibling_count: 0,
            parent: None,
        }
    }
}

impl<'a, T> IdentifierStack<'a, T> {
    pub fn new_with_parent(parent: &'a IdentifierStack<'a, T>) -> Self {
        Self {
            scopes: Vec::new(),
            parent: Some(parent),
            current_scope_depth: parent.current_scope_depth,
            current_sibling_count: parent.current_sibling_count,
        }
    }

    pub fn start_scope(&mut self) -> IdentifierScopeId {
        self.scopes.push((
            IdentifierScope {
                values: HashMap::new(),
            },
            self.current_sibling_count + 1,
        ));

        let count = self.current_sibling_count + 1;

        self.current_scope_depth += 1;
        self.current_sibling_count = 0;

        IdentifierScopeId {
            depth: self.current_scope_depth,
            count,
        }
    }

    pub fn end_scope(&mut self) -> Option<IdentifierScope<T>> {
        self.current_scope_depth = self.current_scope_depth.checked_sub(1)?;

        if let Some((scope, count)) = self.scopes.pop() {
            self.current_sibling_count = count;
            Some(scope)
        } else {
            None
        }
    }

    pub fn add_identifier(
        &mut self,
        ident: Identifier,
        value: T,
    ) -> Result<(), &'static str> {
        self.scopes
            .last_mut()
            .ok_or("no active scope to add identifier to")?
            .0
            .values
            .insert(ident, value);

        Ok(())
    }

    /// Get the type of an identifier as seen by the current scope.
    ///
    /// Also returns the ID of the scope the identifier was found in.
    pub fn get<'c>(
        &'c self,
        ident: &'_ Identifier,
    ) -> Option<(&'c T, IdentifierScopeId)> {
        let mut current_stack = Some(self);
        let mut current_scope_depth = self.current_scope_depth;
        while let Some(stack) = current_stack {
            for scope in stack.scopes.iter().rev() {
                if let Some(value) = scope.0.get(ident) {
                    return Some((
                        value,
                        IdentifierScopeId {
                            depth: current_scope_depth,
                            count: scope.1,
                        },
                    ));
                }
                current_scope_depth -= 1;
            }
            current_stack = stack.parent;
        }
        None
    }

    /// Get the parent identifier stack.
    pub fn parent_stack(&self) -> Option<&'a IdentifierStack<'a, T>> {
        self.parent
    }

    /// End all children scopes to get to the given scope.
    pub fn end_to_scope(
        &mut self,
        scope_id: IdentifierScopeId,
    ) -> Result<(), &'static str> {
        while self.current_scope_id() != scope_id {
            self.end_scope().ok_or("failed to end to given scope")?;
        }
        Ok(())
    }

    /// Get the ID of the current scope.
    pub fn current_scope_id(&self) -> IdentifierScopeId {
        IdentifierScopeId {
            depth: self.current_scope_depth,
            count: self.current_sibling_count,
        }
    }
}

#[test]
fn scopes() {
    let mut x = IdentifierStack::<&'static str>::new();
    let a = x.start_scope();
    x.add_identifier(Identifier::new(2), "foo").unwrap();
    let b = x.start_scope();
    let c = x.start_scope();

    assert!(c.is_inside_of(b));
    assert!(c.is_inside_of(a));
    assert!(!b.is_inside_of(c));

    x.add_identifier(Identifier::new(3), "bar").unwrap();

    let mut y = IdentifierStack::new_with_parent(&x);
    let d = y.start_scope();
    y.add_identifier(Identifier::new(4), "baz").unwrap();
    assert!(d.is_inside_of(a));

    assert_eq!(*y.get(&Identifier::new(4)).unwrap().0, "baz");
    assert_eq!(*y.get(&Identifier::new(3)).unwrap().0, "bar");
    assert_eq!(*y.get(&Identifier::new(2)).unwrap().0, "foo");

    y.end_scope();
    assert_eq!(y.current_scope_id(), c);
    assert!(y.get(&Identifier::new(4)).is_none());
    let e = y.start_scope();

    assert_eq!(d, d);
    assert_ne!(d, e);
    assert!(!d.is_inside_of(e));
    assert!(!e.is_inside_of(d));

    x.end_to_scope(a).unwrap();
    assert!(x.get(&Identifier::new(3)).is_none());
    assert_eq!(*x.get(&Identifier::new(2)).unwrap().0, "foo");
}
