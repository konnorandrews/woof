use num_bigint::BigInt;

use crate::interval::IntegerInterval;

use super::Error;

#[derive(Clone, PartialEq, Debug)]
pub struct Integer {
    width: IntegerWidth,
    refinement: Option<IntegerInterval>,
    base: IntegerInterval,
    signed: IntegerSign,
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct TargetPointerWidth(u16);

impl TargetPointerWidth {
    pub fn new(value: u16) -> Self {
        Self(value)
    }
}

impl Integer {
    pub fn new_refined(
        width: IntegerWidth,
        signed: IntegerSign,
        pointer_width: TargetPointerWidth,
        refinement: IntegerInterval,
    ) -> Result<Self, Error> {
        let base = Self::base_interval_for(width, signed, pointer_width);
        if !refinement.is_subinterval_of(&base) {
            Err("integer type refinement is not contained in it's base type")
        } else if refinement == base {
            Ok(Integer {
                base,
                width,
                refinement: None,
                signed,
            })
        } else {
            Ok(Integer {
                base,
                width,
                refinement: Some(refinement),
                signed,
            })
        }
    }

    pub fn new(
        width: IntegerWidth,
        signed: IntegerSign,
        pointer_width: TargetPointerWidth,
    ) -> Self {
        let base = Self::base_interval_for(width, signed, pointer_width);
        Integer {
            width,
            refinement: None,
            signed,
            base,
        }
    }

    pub fn is_a_refinement_of(&self, other: &Self) -> Result<bool, Error> {
        if self.width != other.width {
            return Err("integer types have different widths");
        }
        if self.signed != other.signed {
            return Err("integer types have different sign-ness");
        }
        match (&self.refinement, &other.refinement) {
            (Some(refinement), Some(other_refinement)) => {
                Ok(refinement.is_subinterval_of(other_refinement))
            }
            (None, Some(other_refinement)) => {
                Ok(false)
            }
            _ => Ok(true)
        }
    }

    pub fn is_same_base(&self, other: &Self) -> bool {
        self.width == other.width && self.signed == other.signed
    }

    pub fn base_interval_for(
        width: IntegerWidth,
        signed: IntegerSign,
        pointer_width: TargetPointerWidth,
    ) -> IntegerInterval {
        use IntegerWidth::*;
        if signed == IntegerSign::Signed {
            match width {
                W8 => IntegerInterval::new(i8::MIN, i8::MAX),
                W16 => IntegerInterval::new(i16::MIN, i16::MAX),
                W32 => IntegerInterval::new(i32::MIN, i32::MAX),
                W64 => IntegerInterval::new(i64::MIN, i64::MAX),
                W128 => IntegerInterval::new(i128::MIN, i128::MAX),
                PointerSize => IntegerInterval::new(
                    -BigInt::from(2).pow(pointer_width.0 as u32 - 1),
                    BigInt::from(2).pow(pointer_width.0 as u32 - 1) - 1,
                ),
            }
        } else {
            match width {
                W8 => IntegerInterval::new(u8::MIN, u8::MAX),
                W16 => IntegerInterval::new(u16::MIN, u16::MAX),
                W32 => IntegerInterval::new(u32::MIN, u32::MAX),
                W64 => IntegerInterval::new(u64::MIN, u64::MAX),
                W128 => IntegerInterval::new(u128::MIN, u128::MAX),
                PointerSize => {
                    IntegerInterval::new(0, BigInt::from(2).pow(pointer_width.0 as _) - 1)
                }
            }
        }
    }

    pub fn width(&self) -> IntegerWidth {
        self.width
    }

    pub fn signed(&self) -> IntegerSign {
        self.signed
    }

    pub fn refinement(&self) -> Option<&IntegerInterval> {
        self.refinement.as_ref()
    }

    pub fn base_interval(&self) -> &IntegerInterval {
        &self.base
    }

    pub fn interval(&self) -> &IntegerInterval {
        if let Some(refinement) = &self.refinement {
            refinement
        } else {
            &self.base
        }
    }
}

#[derive(Clone, PartialEq, Copy, Debug)]
pub enum IntegerWidth {
    /// `u8` / `i8`
    W8,
    /// `u16` / `i16`
    W16,
    /// `u32` / `i32`
    W32,
    /// `u64` / `i64`
    W64,
    /// `u128` / `i128`
    W128,
    /// `usize` / `isize`
    PointerSize,
}

#[derive(Clone, PartialEq, Debug, Copy)]
pub enum IntegerSign {
    /// `+` or `-`
    Signed,
    /// `+` only
    Unsigned,
}

