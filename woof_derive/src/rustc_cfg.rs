use std::{
    collections::HashMap,
    env::{args, var_os, current_exe},
    path::{Path, PathBuf},
    process::Command,
};

#[derive(Debug)]
pub struct RustcCfg {
    pub target_arch: String,
    pub target_features: Vec<String>,
    pub target_os: String,
    pub target_family: Vec<String>,
    pub target_env: Option<String>,
    pub target_endian: Endianness,
    pub target_pointer_width: u16,
    pub target_vendor: String,
    pub target_has_atomic: Vec<String>,
    pub debug_assertions: bool,
    pub panic: String,
}

#[derive(Debug)]
pub enum Endianness {
    Little,
    Big,
}

impl RustcCfg {
    pub fn new() -> Result<RustcCfg, &'static str> {
        let rustc_path = current_exe().map_err(|_| "could not get rustc path")?;

        let mut args: Vec<_> = args().collect();
        if Path::new(&args[0])
            .try_exists()
            .map_err(|_| "failed to check if path exists")?
        {
            args.remove(0);
        }

        let output = Command::new(rustc_path)
            .args(args)
            .args(["--print", "cfg"])
            .output()
            .map_err(|_| "failed to run cargo")?;

        let raw_cfg = String::from_utf8_lossy(&output.stdout);

        let mut target_arch = None;
        let mut target_features = Vec::new();
        let mut target_os = None;
        let mut target_family = Vec::new();
        let mut target_env = None;
        let mut target_endian = None;
        let mut target_pointer_width = None;
        let mut target_vendor = None;
        let mut target_has_atomic = Vec::new();
        let mut debug_assertions = false;
        let mut panic = None;

        for line in raw_cfg.lines() {
            if let Some((name, value)) = line.split_once('=') {
                let value = value.trim_matches('"');
                match name {
                    "target_arch" => target_arch = Some(value),
                    "target_features" => target_features.push(value),
                    "target_os" => target_os = Some(value),
                    "target_family" => target_family.push(value),
                    "target_env" => target_env = Some(value),
                    "target_endian" => target_endian = Some(value),
                    "target_pointer_width" => target_pointer_width = Some(value),
                    "target_vendor" => target_vendor = Some(value),
                    "target_has_atomic" => target_has_atomic.push(value),
                    "panic" => panic = Some(value),
                    _ => {}
                }
            } else if line == "debug_assertions" {
                debug_assertions = true;
            }
        }

        Ok(Self {
            target_arch: target_arch.ok_or("target_arch missing")?.to_owned(),
            target_features: target_features.into_iter().map(str::to_owned).collect(),
            target_os: target_os.ok_or("target_os missing")?.to_owned(),
            target_family: target_family.into_iter().map(str::to_owned).collect(),
            target_env: target_env.and_then(|x| {
                if x.is_empty() {
                    None
                } else {
                    Some(x.to_owned())
                }
            }),
            target_endian: match target_endian.ok_or("target_endian missing")? {
                "little" => Endianness::Little,
                "big" => Endianness::Big,
                _ => return Err("unknown endian")
            },
            target_pointer_width: target_pointer_width
                .ok_or("target_pointer_width missing")?
                .parse()
                .map_err(|_| "failed to parse target_pointer_width")?,
            target_vendor: target_vendor.ok_or("target_arch missing")?.to_owned(),
            target_has_atomic: target_has_atomic.into_iter().map(str::to_owned).collect(),
            panic: panic.ok_or("target_arch missing")?.to_owned(),
            debug_assertions,
        })
    }
}
