mod rustc_cfg;

use proc_macro2::{Ident, TokenStream};
use quote::quote;
use rustc_cfg::RustcCfg;
use syn::{
    parse::Parse,
    parse_macro_input, parse_quote,
    punctuated::Punctuated,
    spanned::Spanned,
    token::{Comma, Mod},
    Attribute, AttributeArgs, Expr, ExprMacro, FnArg, Item, ItemMod, ItemStruct, Macro, ReturnType,
    Stmt,
};

#[proc_macro_attribute]
pub fn woof_context(
    args: proc_macro::TokenStream,
    input: proc_macro::TokenStream,
) -> proc_macro::TokenStream {
    let cfg = RustcCfg::new().unwrap();
    dbg!(cfg);

    // panic!("{:?}", std::env::vars().collect::<Vec<_>>());
    // panic!("{:?}", std::env::var("TARGET"));
    // panic!("{:?}", std::env::args().collect::<Vec<_>>());
    let args = parse_macro_input!(args as AttributeArgs);
    let mut input = parse_macro_input!(input as ItemMod);

    let content = if let Some(content) = &mut input.content {
        &mut content.1
    } else {
        return syn::Error::new_spanned(&input, "Must be a inline module.")
            .to_compile_error()
            .into();
    };

    let mut new_content: Vec<Item> = Vec::new();

    for item in content.iter() {
        match item {
            Item::Use(item) => new_content.push(item.clone().into()),
            Item::Fn(item) => {
                if !item.attrs.iter().all(|attr| is_attribute_allowed(attr)) {
                    return syn::Error::new_spanned(
                        &item,
                        format!("Unsupported attribute on function.\n{}", quote!(#item)),
                    )
                    .to_compile_error()
                    .into();
                }

                if item.sig.constness.is_some() {
                    return syn::Error::new_spanned(
                        &item,
                        format!(
                            "Const functions are currently not allowed.\n{}",
                            quote!(#item)
                        ),
                    )
                    .to_compile_error()
                    .into();
                }

                if item.sig.asyncness.is_some() {
                    return syn::Error::new_spanned(
                        &item,
                        format!(
                            "Async functions are currently not allowed.\n{}",
                            quote!(#item)
                        ),
                    )
                    .to_compile_error()
                    .into();
                }

                if item.sig.unsafety.is_some() {
                    return syn::Error::new_spanned(
                        &item,
                        format!(
                            "All functions in a woof context must be safe.\n{}",
                            quote!(#item)
                        ),
                    )
                    .to_compile_error()
                    .into();
                }

                if item.sig.abi.is_some() {
                    return syn::Error::new_spanned(
                        &item,
                        format!(
                            "Non-standard ABI for functions is not currently allowed.\n{}",
                            quote!(#item)
                        ),
                    )
                    .to_compile_error()
                    .into();
                }

                if item.sig.variadic.is_some() {
                    return syn::Error::new_spanned(
                        &item,
                        format!(
                            "Variadic functions are not currently allowed.\n{}",
                            quote!(#item)
                        ),
                    )
                    .to_compile_error()
                    .into();
                }

                if !(item.sig.generics.lt_token.is_none()
                    && item.sig.generics.gt_token.is_none()
                    && item.sig.generics.params.is_empty()
                    && item.sig.generics.where_clause.is_none())
                {
                    return syn::Error::new_spanned(
                        &item,
                        format!(
                            "Generic functions are not currently allowed.\n{}",
                            quote!(#item)
                        ),
                    )
                    .to_compile_error()
                    .into();
                }

                println!("Woof: detected function: {}", item.sig.ident);

                println!(
                    "Woof: inputs: {:?}",
                    item.sig
                        .inputs
                        .iter()
                        .map(|input| {
                            match input {
                                FnArg::Receiver(_) => unreachable!(),
                                FnArg::Typed(ty) => {
                                    let ty = &ty.ty;
                                    format!("{}", quote!(#ty))
                                }
                            }
                        })
                        .collect::<Vec<_>>()
                );

                println!(
                    "Woof: outputs: {}",
                    match &item.sig.output {
                        ReturnType::Default => "()".into(),
                        ReturnType::Type(_, ty) => format!("{}", quote!(#ty)),
                    }
                );

                // Look for `prove` macro calls for signature.
                let mut signature_proves = Vec::new();
                for statement in &item.block.stmts {
                    if let Stmt::Semi(expr, _) = statement {
                        if let Expr::Macro(ExprMacro {
                            mac: Macro { path, tokens, .. },
                            ..
                        }) = expr
                        {
                            let mut segments = path.segments.iter();
                            let module = segments.next();
                            let name = segments.next();
                            match (module, name) {
                                (None, None) => {}
                                (None, Some(_)) => unreachable!(),
                                (Some(name), None) => {
                                    if name.ident == "prove" {
                                        signature_proves.push(tokens);
                                        continue;
                                    }
                                }
                                (Some(module), Some(name)) => {
                                    if module.ident == "woof" && name.ident == "prove" {
                                        signature_proves.push(tokens);
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                    break;
                }

                println!(
                    "Signature prove: {:?}",
                    signature_proves
                        .iter()
                        .map(|prove| format!("{}", prove))
                        .collect::<Vec<_>>()
                );

                if signature_proves.is_empty() {
                    new_content.push(item.clone().into())
                } else {
                    let name = item.sig.ident.clone();
                    let hidden_name = Ident::new(
                        &format!("__{}", item.sig.ident.to_string()),
                        item.sig.ident.span(),
                    );
                    let arg_types: Punctuated<_, Comma> = item
                        .sig
                        .inputs
                        .iter()
                        .map(|arg| match arg {
                            FnArg::Receiver(receiver) => quote!(#receiver),
                            FnArg::Typed(ty) => {
                                let ty = &ty.ty;
                                quote!(#ty)
                            }
                        })
                        .collect();
                    let args = item.sig.inputs.clone();
                    let ret = item.sig.output.clone();
                    let body = item.block.clone();

                    new_content.push(parse_quote! {
                        pub const #name: ::woof::context::RequiredProof::<1234, unsafe fn(#arg_types) #ret> =
                            unsafe { ::woof::context::RequiredProof::new(#hidden_name) };
                    });
                    new_content.push(parse_quote! {
                        unsafe fn #hidden_name(#args) #ret #body
                    })
                }
            }
            Item::Struct(item) => new_content.push(Item::Struct(item.clone())),
            Item::Impl(item) => new_content.push(Item::Impl(item.clone())),
            _ => todo!("\n{}", quote!(#item)),
        }
    }

    *content = new_content;

    let output = quote!(#input);

    proc_macro::TokenStream::from(output)
}

fn is_attribute_allowed(attr: &Attribute) -> bool {
    if let Some(ident) = attr.path.get_ident() {
        let ident = ident.to_string();
        ALLOWED_ATTRIBUTES.contains(&&*ident)
    } else {
        false
    }
}

// Allowed attributes shouldn't add or change code, it can remove it
static ALLOWED_ATTRIBUTES: &[&str] = &[
    // Documentation has no effect on the code.
    "doc",
    // Only removes code, doesn't change what is there.
    "cfg",
    // Used as marker.
    "automatically_derived",
    // Lint modifiers don't effect the code.
    "allow",
    "warn",
    "deny",
    "forbid",
    // Generates a notice.
    "deprecated",
    // Generates a notice.
    "must_use",
    // Affects linking only.
    "no_mangle",
    "link_section",
    "used",
    "export_name",
    // Affects code gen only.
    "inline",
    "cold",
    "no_builtins",
    "target_feature",
    "track_caller",
    // Marker.
    "non_exhaustive",
];
