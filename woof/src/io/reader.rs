use std::marker::PhantomData;

use crate::Yielder;

use super::{cursor::IoCursor, slice_copy_unchecked};

/// Reader for a buffer.
pub struct IoReader<'a> {
    /// This type acts like it has a borrow to a buffer.
    marker: PhantomData<&'a [u8]>,

    /// Pointer to possibly shared buffer.
    data: *const [u8],

    /// Poitner to possibly shared cursor.
    ///
    /// This cursor's associated buffer is the buffer `data` points to.
    cursor: *mut IoCursor,
}

const NO_BYTES_LEFT: bool = true;
const MAYBE_BYTES_LEFT: bool = false;

impl<'a> IoReader<'a> {
    /// Yield message for when the buffer runs out of data.
    pub const OUT_OF_DATA: &str = "out of data";

    /// # Safety
    /// The values pointed to by `data` and `cursor` must live
    /// for at least the lifetime `'a`.
    ///
    /// The `cursor`'s associated buffer is the buffer `data` points to.
    /// This is required for the always in bounds inveriant of `cursor`.
    ///
    /// No borrows to the values of `data` or `cursor` exist when a method
    /// of the returned `IoReader` is called.
    ///
    /// In turn, `IoReader` will not hold any borrows to `data` or `cursor`
    /// between calls to it's methods, or across awaits.
    pub unsafe fn new(data: *const [u8], cursor: *mut IoCursor) -> Self {
        IoReader {
            marker: PhantomData,
            data,
            cursor,
        }
    }

    /// Check if the resource is empty.
    ///
    /// the returned value is a best guess. If this returns `false`,
    /// then the caller can assume that no more data is available from the resouce.
    /// This cannot be used for unsafe invariants.
    pub fn is_empty(&self) -> bool {
        let (cursor, _) = self.borrow_inner();
        cursor.is_empty() && cursor.closed
    }

    pub async fn read_u8(&self, yielder: &Yielder) -> Option<u8> {
        if self.yield_until_has_n_bytes(yielder, 1).await == NO_BYTES_LEFT {
            return None;
        }

        let (cursor, data) = self.borrow_inner();

        let index = cursor.read_index();
        let byte = data[index];
        unsafe { cursor.set_read_index_unchecked(index + 1) };
        Some(byte)
    }

    pub async fn read_into_exact(&self, yielder: &Yielder, buf: &mut [u8]) -> bool {
        if self.yield_until_has_n_bytes(yielder, buf.len()).await == NO_BYTES_LEFT {
            return false;
        }

        let (cursor, data) = self.borrow_inner();

        let count = buf.len();
        let src_start = cursor.read_index();

        unsafe { slice_copy_unchecked(data, src_start, buf, 0, count) };

        unsafe { cursor.set_read_index_unchecked(src_start + count) };

        true
    }

    pub fn read_into(&self, buf: &mut [u8]) -> Option<usize> {
        let (cursor, data) = self.borrow_inner();

        let bytes_left = cursor.len();

        if bytes_left == 0 && cursor.closed {
            return None;
        }

        let count = buf.len().min(bytes_left);
        let src_start = cursor.read_index();

        unsafe { slice_copy_unchecked(data, src_start, buf, 0, count) };

        unsafe { cursor.set_read_index_unchecked(src_start + count) };

        Some(count)
    }

    async fn yield_until_has_n_bytes(&self, yielder: &Yielder, n: usize) -> bool {
        // Keep yielding until we get data or the buffer becomes closed.
        //
        // Note, we don't hold a borrow to the cursor across the await.
        loop {
            match self.has_n_bytes_is_closed(n) {
                (true, _) => return MAYBE_BYTES_LEFT,
                (false, true) => return NO_BYTES_LEFT,
                (false, false) => yielder.yield_now(Some(Self::OUT_OF_DATA)).await,
            }
        }
    }

    /// Check if the buffer has `n` bytes ready to read, and if any more data
    /// is expected.
    ///
    /// This is used for checking if a yield should happen.
    fn has_n_bytes_is_closed(&self, n: usize) -> (bool, bool) {
        let (cursor, _) = self.borrow_inner();
        (cursor.len() >= n, cursor.closed)
    }

    /// Borrow the cursor and buffer the this reader is for.
    ///
    /// This method is not unsafe because this module's code upholds the inner
    /// unsafe invariants.
    ///
    /// The returned value from this method **must** not live longer than
    /// the body of a public method, or be held across an await anywhere.
    ///
    /// This method **must** not be called while a return from this method
    /// exists. The lifetime bounds are only here to add a layer of checking.
    /// They do **not** prevent misuse by themselves.
    fn borrow_inner<'b>(&'b self) -> (&'b mut IoCursor, &'b [u8]) {
        // SAFETY:
        // This function is only called within this file.
        //
        // The `new` method invariants allow this unsafe to know no outstanding
        // &mut exist to the value `self.cursor` or `self.data` point to.
        //
        // The borrow `cursor` and `data` will be dropped by the end of the calling pub
        // method. The return value is not held across an await.
        unsafe { (&mut *self.cursor, &*self.data) }
    }
}

impl<'a> std::io::Read for &IoReader<'a> {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        match self.read_into(buf) {
            // No bytes were written, but the buffer isn't closed.
            Some(0) => Err(std::io::Error::new(
                std::io::ErrorKind::WouldBlock,
                "No data left, but buffer not closed",
            )),

            Some(len) => Ok(len),

            // No bytes written and buffer is closed.
            None => Ok(0),
        }
    }
}
