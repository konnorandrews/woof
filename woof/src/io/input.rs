
pub struct InputBuffer<'a> {
    marker: PhantomData<&'a [u8]>,
    data: *const [u8],
    cursor: UnsafeCell<IoCursor>,
}

