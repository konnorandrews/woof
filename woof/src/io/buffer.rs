use std::{cell::UnsafeCell, marker::PhantomData, mem::MaybeUninit};

use super::{cursor::IoCursor, reader::IoReader, slice_copy_unchecked, slice_move_unchecked};

const DEFAULT_BUF_SIZE: usize = 512;

pub struct IoBuffer<'a> {
    /// This type acts like it manages a `&mut [u8]`.
    marker: PhantomData<&'a mut [u8]>,

    /// Pointer to the buffer this type manages.
    data: *mut [u8], // Use NonNull

    /// Cursor into `data`.
    ///
    /// `data` is the associated buffer of `cursor`.
    cursor: UnsafeCell<IoCursor>,
}

impl<'a> std::fmt::Debug for IoBuffer<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // SAFETY:
        // The `new` method of `IoReader` allow this unsafe to know no outstanding
        // &mut exist to the value `self.cursor` points to because of a reader.
        //
        // The borrow `cursor` and `data` will be dropped at the end of this method.
        // No calls to a reader's methods is made.
        let (cursor, data): (&IoCursor, &[u8]) = unsafe { (&*self.cursor.get(), &*self.data) };

        f.debug_struct("IoBuffer")
            .field("data", &data)
            .field("cursor", &cursor)
            .finish()
    }
}

impl<'a> IoBuffer<'a> {
    pub fn new<T>(buffer: &'a mut T) -> Self
    where
        T: AsMut<[u8]> + ?Sized,
    {
        let data = buffer.as_mut();

        let cursor = IoCursor::new();

        Self {
            marker: PhantomData,
            data,
            cursor: UnsafeCell::new(cursor),
        }
    }

    pub fn reader<'b>(&'b self) -> IoReader<'b> {
        // SAFETY:
        // `self.data` lives for `'a` which is longer or equal to the returned IoReader's `'b`.
        //
        // `IoBuffer` holds no borrows to it's `self.data`'s value or the inner value
        // of `self.cursor` across calls to the returned `IoReader`'s methods.
        unsafe { IoReader::new(self.data, self.cursor.get()) }
    }

    pub fn set_close(&self, value: bool) {
        let (cursor, _) = self.borrow_inner();
        cursor.closed = value;
    }

    pub fn compact(&self) {
        let (cursor, data) = self.borrow_inner();

        let count = cursor.len();
        let src_start = cursor.read_index();
        let dest = 0;

        if src_start == 0 {
            // We don't need to compact if already compact.
            return;
        }

        // Copy unread data to the beginning of the slice.
        //
        // SAFETY:
        // "src must be valid for reads of count * size_of::<T>() bytes." - using
        // the inveriant on IoCursor that `cursor.read_index() + cursor.len() - 1`
        // will always be a valid index. We know we can read from `src_start`
        // `count` bytes.
        //
        // "dst must be valid for writes of count * size_of::<T>() bytes." -
        // because `dest` is zero and `count` can't be larger than the length of data
        // we know that `dst` is valid for writes of `count` in length.
        //
        // "Both src and dst must be properly aligned." - both `src_ptr` and `dest_ptr`
        // are derived from a properly aligned `ptr`.
        unsafe { slice_move_unchecked(data, src_start, dest, count) };

        // Move cursor to the copied data.

        // We shifted forward by `read_index` bytes.
        cursor.position += cursor.read_index();

        // SAFETY:
        // `0` is always less than or equal to the current valud of `cursor.write_index()`.
        unsafe {
            cursor.set_read_index_unchecked(0);
        }

        // SAFETY:
        // We just set `cursor.read_index()` to `0` so `count` will always be greater or equal to
        // it, and `count` is less than or equal to the length of the associated buffer `data`.
        unsafe { cursor.set_write_index_unchecked(count) }
    }

    /// Borrow the cursor and buffer the this reader is for.
    ///
    /// This method is not unsafe because this module's code upholds the inner
    /// unsafe invariants.
    ///
    /// The returned value from this method **must** not live longer than
    /// the body of a public method, or be held across an await anywhere.
    ///
    /// This method **must** not be called while a return from this method
    /// exists. The lifetime bounds are only here to add a layer of checking.
    /// They do **not** prevent misuse by themselves.
    fn borrow_inner<'b>(&'b self) -> (&'b mut IoCursor, &'b mut [u8]) {
        // SAFETY:
        // This function is only called within this file.
        //
        // The `new` method invariants allow this unsafe to know no outstanding
        // &mut exist to the value `self.cursor` or `self.data` point to.
        //
        // The borrow `cursor` and `data` will be dropped by the end of the calling pub
        // method. The return value is not held across an await.
        unsafe { (&mut *self.cursor.get(), &mut *self.data) }
    }

    pub fn fill_from_reader<T>(&self, reader: &mut T) -> std::io::Result<usize>
    where
        T: std::io::Read + ?Sized,
    {
        let mut written_len = 0;
        let buf = &mut [0u8; DEFAULT_BUF_SIZE];

        loop {
            let mut extra_bytes: usize;
            {
                let (cursor, data) = self.borrow_inner();
                extra_bytes = writable_len(cursor, data);
                drop((cursor, data));
            }

            let count = buf.len().min(extra_bytes);

            let result = reader.read(unsafe { buf.get_unchecked_mut(0..count) });

            match result {
                Ok(0) => return Ok(written_len),
                Ok(len) => {
                    let (cursor, data) = self.borrow_inner();
                    extra_bytes = writable_len(cursor, data);

                    if len > extra_bytes {
                        return Err(std::io::Error::new(
                            std::io::ErrorKind::Other,
                            "Read more bytes than buffer has room to store.",
                        ));
                    }

                    let write_index = cursor.write_index();

                    unsafe { slice_copy_unchecked(buf, 0, data, write_index, len) };

                    unsafe { cursor.set_write_index_unchecked(write_index + len) };

                    written_len += len;

                    drop((cursor, data));
                }
                Err(e) if e.kind() == std::io::ErrorKind::Interrupted => continue,
                Err(e) => return Err(e),
            };
        }
    }
}

fn writable_len(cursor: &IoCursor, data: &mut [u8]) -> usize {
    // Note, known to not underflow because of the inveriant on `IoCusror`.
    data.len() - cursor.write_index()
}

impl<'a> std::io::Write for &IoBuffer<'a> {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        let (cursor, data) = self.borrow_inner();

        let extra_bytes = writable_len(cursor, data);

        let count = buf.len().min(extra_bytes);
        let src_start = 0;
        let dest = cursor.write_index();

        // Copy unread data to the beginning of the slice.
        //
        // SAFETY:
        // "src must be valid for reads of count * size_of::<T>() bytes." -
        // `count` is bounded by `buf.len()` and `src_start` is always zero. So
        // `src` is always valid for reads.
        //
        // "dst must be valid for writes of count * size_of::<T>() bytes." -
        // `count` is bounded by `data.len() - cursor.write_index()` which
        // means `cursor.write_index() + count - 1` is always within `data`.
        //
        // "Both src and dst must be properly aligned." - both `src_ptr` and `dest_ptr`
        // are derived from a properly aligned pointers.
        unsafe {
            let src_ptr = buf.as_ptr().add(src_start);
            let dest_ptr = data.as_mut_ptr().add(dest);
            std::ptr::copy(src_ptr, dest_ptr, count);
        }

        // SAFETY:
        // The new value is always bigger or equal to the original `write_index`.
        unsafe { cursor.set_write_index_unchecked(cursor.write_index() + count) }

        Ok(count)
    }

    fn flush(&mut self) -> std::io::Result<()> {
        // Nothing to flush to.
        Ok(())
    }
}
