/// A cursor into a IO buffer.
///
/// A `IoCusror` always has an associated buffer managed by another
/// type. That buffer may contain only part of the entire resource.
///
/// `position` is the position into the overall resource, not
/// the position in the associated buffer.
///
/// `closed` flags if a reader can expect more data to be placed
/// in the associated buffer from the resource. `false` means
/// there is possibly still data left in the resource that isn't in
/// the associated buffer. `true` means there probably isn't any more
/// data outside of what is in the associated buffer.
///
/// `read_index` (private) is the index into the associated buffer
/// readers will read from.
///
/// `write_index` (private) is the index into the associated buffer
/// writers will write to.
///
/// # Safety
///
/// This type maintains an invariant of
/// `0 <= read_index <= write_index <= associated_buf.len()`.
/// This invariant can be used by unsafe code interacting with the
/// associated buffer.
#[derive(Debug)]
pub struct IoCursor {
    read_index: usize,
    write_index: usize,
    pub position: usize,
    pub closed: bool,
}

impl IoCursor {
    /// Create an empty cursor.
    ///
    /// The returned cursor is safe for use for any buffer.
    /// Once a `set_x_unchecked` method is used then the `IoCursor`
    /// can only be used for the buffer the caller chooses.
    ///
    /// - `position` = 0
    /// - `closed` = false
    /// - `read_index` = 0
    /// - `write_index` = 0
    pub fn new() -> Self {
        Self {
            read_index: 0,
            write_index: 0,
            position: 0,
            closed: false,
        }
    }

    /// # Safety
    /// `value` must be less than or equal to the current `self.write_index()` value.
    pub unsafe fn set_read_index_unchecked(&mut self, value: usize) {
        self.read_index = value;
    }

    /// # Safety
    /// `value` must be greater than or equal to the current `self.read_index()` value.
    ///
    /// `value` must be less than or equal to the length of the associated buffer.
    pub unsafe fn set_write_index_unchecked(&mut self, value: usize) {
        self.write_index = value;
    }

    /// Index readers can read from.
    ///
    /// This index is known to always be in bounds of the associated buffer.
    pub fn read_index(&self) -> usize {
        self.read_index
    }

    /// Index writers can write to.
    ///
    /// This index is known to always be in bounds of the associated buffer.
    pub fn write_index(&self) -> usize {
        self.write_index
    }

    /// Return the number of bytes left to read.
    ///
    /// The index `self.read_index() + self.len() - 1` will **always** be in the
    /// bounds of the associated buffer.
    pub fn len(&self) -> usize {
        // Note, known to not underflow because of the `set_x_unchecked` methods.
        self.write_index - self.read_index
    }

    /// Check if there are any items in the associated buffer the readers haven't read yet.
    pub fn is_empty(&self) -> bool {
        self.write_index == self.read_index
    }
}
