pub mod buffer;
pub mod cursor;
pub mod reader;

/// Move bytes in slice.
///
/// # Safety
/// `(src_start + count) <= slice.len()` must hold.
/// `(dest_start + count) <= slice.len()` must hold.
unsafe fn slice_move_unchecked(
    slice: &mut [u8],
    src_start: usize,
    dest_start: usize,
    count: usize,
) {
    // Derive both `src_ptr` and `dest_ptr` from the same loan to not invalid
    // one or the other.
    let ptr = slice.as_mut_ptr();
    let src_ptr = ptr.add(src_start);
    let dest_ptr = ptr.add(dest_start);

    // SAFETY:
    // `src_ptr` and `dest_ptr` are well aligned.
    // `src_ptr` and `dest_ptr` are valid for read/write respectively
    // for `count` bytes by the safety requirements of this function.
    std::ptr::copy(src_ptr, dest_ptr, count);
}

/// Copy bytes from one slice to another.
///
/// # Safety
/// `(src_start + count) <= src.len()` must hold.
/// `(dest_start + count) <= dest.len()` must hold.
unsafe fn slice_copy_unchecked(
    src: &[u8],
    src_start: usize,
    dest: &mut [u8],
    dest_start: usize,
    count: usize,
) {
    let src_ptr = src.as_ptr().add(src_start);
    let dest_ptr = dest.as_mut_ptr().add(dest_start);

    // SAFETY:
    // `src_ptr` and `dest_ptr` are well aligned.
    // `src_ptr` and `dest_ptr` are valid for read/write respectively
    // for `count` bytes by the safety requirements of this function.
    std::ptr::copy_nonoverlapping(src_ptr, dest_ptr, count);
}
