pub mod context;
mod coroutine;
mod io;

use std::{
    cell::UnsafeCell,
    marker::PhantomData,
    ptr::{addr_of, addr_of_mut},
};

pub use coroutine::{Coroutine, CoroutineStatus, Yielder};

pub use woof_derive::woof_context as woof;

pub type Result<T, E = &'static str> = std::result::Result<T, E>;

pub use io::{buffer::IoBuffer, cursor::IoCursor, reader::IoReader};

#[macro_export]
macro_rules! prove {
    ($($coroutine:tt)*) => {{}};
}

#[macro_export]
macro_rules! pin_mut {
    ($name:ident) => {
        // Make sure the value is owned by this scope.
        let mut $name = $name;

        // SAFETY:
        // By shadowing the binding of the value we prevent any code from
        // moving it out of this scope.
        #[allow(unused_mut)]
        let mut $name = unsafe { ::core::pin::Pin::new_unchecked(&mut $name) };
    };
}
