/// Wrapper encoding the proof required to use a value.
///
/// `P` is a encoded proof needed by `T`'s definition to safely use `T`.
///
/// This type is used to allow multiple Woof contexts to interact with each other
/// in a safe way. This is done by having the compiler's type checking do
/// the final proof verification.
#[derive(Copy, Clone, Debug)]
pub struct RequiredProof<const P: u128, T>(T);

impl<const P: u128, T> RequiredProof<P, T> {
    /// # Safety
    /// The encoded proof `P` must contain all safety
    /// invariants to safely use the `value`.
    pub const unsafe fn new(value: T) -> Self {
        Self(value)
    }
}

/// # Safety
/// The encoded proof `P` must have been proven in this current context.
/// If the compiler ever allows `P` to be inferred, then it is unsafe to allow it to
/// for this function.
///
/// If the above requirement is satisfied, then the returned value is safe to use even in
/// `unsafe` contexts. The existence of `P` acts as the safety comment for the usage
/// of the returned value.
///
/// The returned value **must** not escape the context the proof `P` is valid for.
pub unsafe fn prove_usage<const P: u128, T>(value: RequiredProof<P, T>) -> T {
    value.0
}

#[test]
fn demo() {
    // Example code expansion.
    mod demo {
        #![allow(non_upper_case_globals)]

        use super::*;

        // Wrapped public item with it's safety requirements encoded
        // by `1234` namely b cannot be 0 (this is just an example value).
        //
        // Outside of this module it is not possible to get a handle to __foo
        // without calling `with_usage`.
        pub const foo: RequiredProof<1234, unsafe fn(i32, i32) -> i32> =
            unsafe { RequiredProof::new(__foo) };

        // This function has a safety requirement that `b` is not zero for this example.
        unsafe fn __foo(a: i32, b: i32) -> i32 {
            a / b
        }
    }

    // By calling prove_usage with `1234` we promise that when we call `foo`,
    // the second argument `b` will not be zero.
    let foo = unsafe { prove_usage::<1234, _>(demo::foo) };

    // SAFETY:
    // The above use of `prove_usage` is the safety we need to call `foo`.
    unsafe {
        (foo)(1, 3);
    }
}
