use std::{
    cell::Cell,
    future::Future,
    pin::Pin,
    task::{Context, Poll},
};

pub struct Yielder {
    yield_message: Cell<Option<&'static str>>,
}

impl Yielder {
    /// Create a new `Coroutine` for a coroutine-like async function
    /// to yeild to the caller.
    pub fn new() -> Self {
        Self {
            yield_message: Cell::new(None),
        }
    }

    /// Create a future that when awaited will yeild to the caller.
    ///
    /// `message` is an optional message to pass to the caller.
    pub fn yield_now(&self, message: Option<&'static str>) -> impl Future<Output = ()> {
        // Set the yield message for the caller of the coroutine to read.
        self.yield_message.set(message);

        // Return a future that will return `Pending` once.
        YieldNow::new()
    }

    /// Message from last `yield_now` call.
    fn last_message(&self) -> Option<&'static str> {
        self.yield_message.get()
    }
}

pub enum CoroutineStatus<T> {
    Done(T),
    Suspended(Option<&'static str>),
}

impl<T> CoroutineStatus<T> {
    #[track_caller]
    pub fn unwrap(self) -> T {
        match self {
            CoroutineStatus::Done(value) => value,
            CoroutineStatus::Suspended(message) => {
                panic!(
                    "Coroutine unexpectedly suspended: {}",
                    message.unwrap_or("No suspend message given.")
                )
            }
        }
    }
}

pub struct Coroutine<'a, T> {
    yielder: &'a Yielder,
    future: Pin<&'a mut dyn Future<Output = T>>,
}

impl<'a, T> Coroutine<'a, T> {
    pub fn new(future: Pin<&'a mut dyn Future<Output = T>>, yielder: &'a Yielder) -> Self {
        Self { future, yielder }
    }

    pub fn poll(&mut self) -> CoroutineStatus<T> {
        let waker = nothing_waker::make_waker();
        let mut context = Context::from_waker(&waker);
        match self.future.as_mut().poll(&mut context) {
            Poll::Ready(value) => CoroutineStatus::Done(value),
            Poll::Pending => CoroutineStatus::Suspended(self.yielder.last_message()),
        }
    }
}

mod nothing_waker {
    use std::task::{RawWaker, RawWakerVTable, Waker};

    pub fn make_waker() -> Waker {
        let raw_waker = RawWaker::new(std::ptr::null(), &VTable);

        // SAFETY:
        // `from_raw` is only safe if "contract defined in RawWaker’s and
        // RawWakerVTable’s documentation is not upheld". Follows is why
        // this is the case.
        //
        // "Calling one of the contained functions using any other data
        // pointer will cause undefined behavior." - none of the functions
        // in the v-table are ever called outside of a `RawWaker`.
        //
        // "These functions must all be thread-safe" - none of the functions
        // mutate anything or read anything not Sync and therefore are thread safe.
        //
        // "The implementation of this function must retain all resources
        // that are required for this additional instance of a RawWaker and
        // associated task. Calling wake on the resulting RawWaker should
        // result in a wakeup of the same task that would have been awoken
        // by the original RawWaker." - No resources are needed for the new
        // `RawWaker`, and the created `RawWaker` will "flag" that the future
        // whould be awoken because this waker doesn't actually flag that at all.
        //
        // "It must wake up the task associated with this RawWaker." - The
        // `Coroutine` struct will always re-poll the same future. The caller of
        // `poll` may decide to drop `Coroutine` but that is fine.
        //
        // "must make sure to release any resources that are associated
        // with this instance of a RawWaker and associated task." - The created
        // raw_waker has no resources to release.
        //
        // "must not consume the provided data pointer." - The `wake_by_ref` does
        // not consume the data pointer.
        unsafe { Waker::from_raw(raw_waker) }
    }

    static VTable: RawWakerVTable =
        RawWakerVTable::new(impl_clone, impl_wake, impl_wake_by_ref, impl_drop);

    fn impl_clone(value: *const ()) -> RawWaker {
        RawWaker::new(value, &VTable)
    }

    fn impl_wake(value: *const ()) {}

    fn impl_wake_by_ref(value: *const ()) {}

    fn impl_drop(value: *const ()) {}
}

struct YieldNow {
    is_ready: bool,
}

impl YieldNow {
    fn new() -> Self {
        Self { is_ready: false }
    }
}

impl Future for YieldNow {
    type Output = ();

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        if self.is_ready {
            Poll::Ready(())
        } else {
            // Mark as ready for next poll.
            self.is_ready = true;

            // Flag to be woken up immediately.
            cx.waker().wake_by_ref();

            // Pause execution of any caller futures.
            Poll::Pending
        }
    }
}
