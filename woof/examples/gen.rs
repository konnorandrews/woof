fn main() {
    let x = unsafe { (woof::context::prove_usage::<1234, _>(other::test))(1, 2) };
    // dbg!(x);
}

#[woof::woof]
mod other {
    pub fn test(a: i32, b: i32) -> i32 {
        woof::prove!(a != b);
        woof::prove!(return < 5);
        a + b
    }
}
