use std::io::Write;

// #[woof::woof]
mod demo {
    pub use woof::*;

    /// Test B
    pub async fn parse(yielder: &Yielder, src: &IoReader<'_>) -> Result<u32> {
        let mut val = 0;

        while let Some(mut c) = src.read_u8(yielder).await {
            if (c < 0x30) || (0x39 < c) {
                return Err("not a digit");
            }

            c -= 0x30;

            if val < 429_496_729 {
                val = (10 * val) + (c as u32);
                continue;
            } else if (val > 429_496_729) || (c > 5) {
                return Err("too large");
            }

            // woof::assert!(false);

            val = (10 * val) + (c as u32);
        }

        Ok(val)
    }
}

fn run(input: &str) {
    use woof::*;

    let mut cursor = std::io::Cursor::new(input);

    // let mut reader_pos = 0;
    // let mut reader = IoReader::from_slice(input, &mut reader_pos);
    let mut buffer = [0u8; 4];
    let buffer = IoBuffer::new(&mut buffer);
    let reader = buffer.reader();

    let yielder = Yielder::new();
    let future = demo::parse(&yielder, &reader);
    pin_mut!(future);
    let mut co = Coroutine::new(future, &yielder);

    loop {
        match co.poll() {
            CoroutineStatus::Done(value) => {
                println!("{:?}", value);
                break;
            }
            CoroutineStatus::Suspended(Some(IoReader::OUT_OF_DATA)) => {
                buffer.compact();
                buffer.fill_from_reader(&mut cursor).unwrap();
                if cursor.position() as usize == input.len() {
                    buffer.set_close(true);
                }
                // dbg!(&buffer);
            }
            CoroutineStatus::Suspended(message) => {
                println!("Unexpected suspend reason: {:?}", message)
            }
        }
    }

    // match co.poll().unwrap() {
    //     Ok(value) => println!("{}", value),
    //     Err(message) => println!("error: {}", message),
    // }
}

fn main() {
    run("");
    run("0");
    run("12");
    run("56789");
    run("4294967295"); // (1<<32) - 1, aka UINT32_MAX.
    run("4294967296"); // (1<<32).
    run("123456789012");
    run("1234abc643");
}
