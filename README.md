# Woof

Experimental [Wuffs](https://github.com/google/wuffs) like environment for Rust.

This project is not a re-implementation of the Wuffs language. Instead, this project aims to bring the compile time checking of Wuffs to Rust as a proc macro and other utilities.
